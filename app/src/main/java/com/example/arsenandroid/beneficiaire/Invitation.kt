package com.example.arsenandroid.beneficiaire

import android.widget.ExpandableListView
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.invitation.*

import java.util.ArrayList
import java.util.HashMap

class Invitation : AppCompatActivity() {

    lateinit var listView: ExpandableListView
    lateinit var listAdapter: ExpandableListAdapter
    lateinit var listDataHeader: MutableList<String>
    lateinit var listHash: HashMap<String, List<String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.invitation)

        listView = findViewById(R.id.lvExp) as ExpandableListView
        initData()
        listAdapter = ExpandableListAdapter(this, listDataHeader, listHash)
        listView.setAdapter(listAdapter)

        skipButton.setOnClickListener {
            onBackPressed()
        }
    }

    fun initData() {
        listDataHeader = ArrayList()
        listHash = HashMap()
        listDataHeader.add("Adultes")
        listDataHeader.add("Enfants")

        val emDev = ArrayList<String>()
        emDev.add("C'est tout moi")

        val anStu = ArrayList<String>()
        anStu.add("C'est tout moi")

        listHash[listDataHeader[0]] = emDev
        listHash[listDataHeader[1]] = anStu
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        listView.setIndicatorBounds(listView.right - 60, listView.width - 10)
    }

}
