package com.example.arsenandroid.beneficiaire

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.arsenandroid.R
import com.example.arsenandroid.view.sign.AddFamily
//import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kotlinx.android.synthetic.main.list_benef_item_enfant.view.*
import kotlinx.android.synthetic.main.list_item_benef.view.*

import java.util.HashMap

class ExpandableListAdapter(
    val context: Context,
    val listDataHeader: List<String>,
    val listHashMap: HashMap<String, List<String>>
) : BaseExpandableListAdapter() {

    /*lateinit var myList: ListView
    lateinit var arrayList: ArrayList<String>
    lateinit var adapter:ArrayAdapter<String>*/

    var check = 0

    //private val childViewHolder: ChildViewHolder? = null

    override fun getGroupCount(): Int {
        return listDataHeader.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return listHashMap[listDataHeader[groupPosition]]!!.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return listDataHeader[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return listHashMap[listDataHeader[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition) as String
        var groupType = getGroupType(groupPosition)
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (convertView == null || convertView.tag != groupType) {
            convertView = inflater.inflate(R.layout.list_item_genre, null)

            val IbListHeader = convertView!!.findViewById<View>(R.id.listHeader) as TextView
            val img = convertView.findViewById<View>(R.id.genreImg) as ImageView
            val arrow = convertView.findViewById<View>(R.id.arrowImg) as ImageView
            IbListHeader.setTypeface(null, Typeface.BOLD)
            IbListHeader.text = headerTitle
            if (groupPosition == 0) {
                check = 1
                img.setImageResource(R.drawable.ic_man)
            } else {
                check = 2
                img.setImageResource(R.drawable.ic_boy)
            }
            if (isExpanded) {
                arrow.setImageResource(R.drawable.ic_accordion_moins)
            } else {
                arrow.setImageResource(R.drawable.ic_accordion_plus)
            }

        }

        return convertView!!

    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup)
            : View {

        var convertView = convertView
        var childText = getChild(groupPosition, childPosition) as String
        val headerTitle = getGroup(groupPosition) as String
        var itemType: Int = getChildType(groupPosition, childPosition)
        val inflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (convertView == null || convertView.tag != itemType) {

            when(itemType){
                0 -> {
                    convertView = inflater.inflate(R.layout.list_item_benef, null)
                    convertView.tag = itemType

                    /*convertView!!.nomComplet.tag = childViewHolder
                    convertView!!.carteIdentity.tag = childViewHolder
                    convertView!!.telMail.tag = childViewHolder*/

                    val validateLayout1 = convertView!!.findViewById<LinearLayout>(R.id.benefValidate1)
                    val validateLayout2 = convertView!!.findViewById<LinearLayout>(R.id.benefValidate2)
                    val validateLayout3 = convertView!!.findViewById<LinearLayout>(R.id.benefValidate3)
                    val validateLayout4 = convertView!!.findViewById<LinearLayout>(R.id.benefValidate4)
                    val validateLayout5 = convertView!!.findViewById<LinearLayout>(R.id.benefValidate5)



                    val parent1: ViewGroup = validateLayout1.parent as ViewGroup
                    parent1.removeView(validateLayout1)
                    val parent2: ViewGroup = validateLayout2.parent as ViewGroup
                    parent2.removeView(validateLayout2)
                    val parent3: ViewGroup = validateLayout3.parent as ViewGroup
                    parent3.removeView(validateLayout3)
                    val parent4: ViewGroup = validateLayout4.parent as ViewGroup
                    parent4.removeView(validateLayout4)
                    val parent5: ViewGroup = validateLayout5.parent as ViewGroup
                    parent5.removeView(validateLayout5)

                    var nbClick = 0

                    fun perform(n: String){
                        var parent: String = "parent"+n
                        var benef: String = "benefValidateName"
                        var i: String = "convertView" + "." + benef + n
                    }

                    convertView.sendInvite.setOnClickListener {

                        if (convertView!!.nomComplet.text.toString() == "" || convertView!!.carteIdentity.text.toString() == "" || convertView!!.telMail.text.toString() == "") {
                            Toast.makeText(context, "Tous les champs sont obligatoires", Toast.LENGTH_SHORT).show()
                        } else {

                            nbClick += 1
                            if (nbClick < AddFamily.oldNb + 1) {
                                if (nbClick == 1) {

                                    parent1.addView(validateLayout1)
                                    convertView!!.benefValidateName1.text = convertView!!.nomComplet.text.toString()
                                    convertView!!.benefValidateCarteId1.text = "-  " + convertView!!.carteIdentity.text.toString()
                                    convertView!!.nomComplet.text = null
                                    convertView!!.carteIdentity.text = null
                                    convertView!!.telMail.text = null

                                } else if (nbClick == 2) {

                                    parent2.addView(validateLayout2)
                                    convertView!!.benefValidateName2.text = convertView!!.nomComplet.text.toString()
                                    convertView!!.benefValidateCarteId2.text = "-  " + convertView!!.carteIdentity.text.toString()
                                    convertView!!.nomComplet.text = null
                                    convertView!!.carteIdentity.text = null
                                    convertView!!.telMail.text = null

                                } else if (nbClick == 3) {

                                    parent3.addView(validateLayout3)
                                    convertView!!.benefValidateName3.text = convertView!!.nomComplet.text.toString()
                                    convertView!!.benefValidateCarteId3.text = "-  " + convertView!!.carteIdentity.text.toString()
                                    convertView!!.nomComplet.text = null
                                    convertView!!.carteIdentity.text = null
                                    convertView!!.telMail.text = null

                                } else if (nbClick == 4) {

                                    parent4.addView(validateLayout4)
                                    convertView!!.benefValidateName4.text = convertView!!.nomComplet.text.toString()
                                    convertView!!.benefValidateCarteId4.text = "-  " + convertView!!.carteIdentity.text.toString()
                                    convertView!!.nomComplet.text = null
                                    convertView!!.carteIdentity.text = null
                                    convertView!!.telMail.text = null

                                } else if (nbClick == 5) {

                                    parent5.addView(validateLayout5)
                                    convertView!!.benefValidateName5.text = convertView!!.nomComplet.text.toString()
                                    convertView!!.benefValidateCarteId5.text = "-  " + convertView!!.carteIdentity.text.toString()
                                    convertView!!.nomComplet.text = null
                                    convertView!!.carteIdentity.text = null
                                    convertView!!.telMail.text = null
                                }

                            } else {

                                val myLayout = convertView!!.findViewById<LinearLayout>(R.id.myLayout)
                                val parent: ViewGroup = myLayout.parent as ViewGroup
                                parent.removeView(myLayout)
                                convertView!!.sendInvite.visibility = View.INVISIBLE
                                convertView!!.invitTextId.visibility = View.INVISIBLE
                                Toast.makeText(
                                    context,
                                    "Vous avez atteint le nombres de bénéficiaires adultes pour lesquels vous avez payez",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }

                }
                1 -> {
                    convertView = inflater.inflate(R.layout.list_benef_item_enfant, null)
                    convertView.tag = itemType

                    /*convertView!!.nomCompletEnf.tag = childViewHolder
                    convertView!!.carteIdentityEnf.tag = childViewHolder
                    convertView!!.telMailEnf.tag = childViewHolder*/

                    val validateLayout1 = convertView!!.findViewById<LinearLayout>(R.id.benefValidChild1)
                    val validateLayout2 = convertView!!.findViewById<LinearLayout>(R.id.benefValidChild2)
                    val validateLayout3 = convertView!!.findViewById<LinearLayout>(R.id.benefValidChild3)
                    val validateLayout4 = convertView!!.findViewById<LinearLayout>(R.id.benefValidChild4)
                    val validateLayout5 = convertView!!.findViewById<LinearLayout>(R.id.benefValidChild5)


                    val parent1: ViewGroup = validateLayout1.parent as ViewGroup
                    parent1.removeView(validateLayout1)
                    val parent2: ViewGroup = validateLayout2.parent as ViewGroup
                    parent2.removeView(validateLayout2)
                    val parent3: ViewGroup = validateLayout3.parent as ViewGroup
                    parent3.removeView(validateLayout3)
                    val parent4: ViewGroup = validateLayout4.parent as ViewGroup
                    parent4.removeView(validateLayout4)
                    val parent5: ViewGroup = validateLayout5.parent as ViewGroup
                    parent5.removeView(validateLayout5)

                    var nbClick = 0

                    convertView.sendInviteEnf.setOnClickListener {

                        if (convertView!!.nomCompletEnf.text.toString() == "" || convertView!!.carteIdentityEnf.text.toString() == "" || convertView!!.telMailEnf.text.toString() == "") {
                            Toast.makeText(context, "Tous les champs sont obligatoires", Toast.LENGTH_SHORT).show()
                        } else {

                            nbClick += 1
                            if (nbClick < AddFamily.childNb + 1) {
                                if (nbClick == 1) {

                                    parent1.addView(validateLayout1)
                                    convertView!!.benefValidChildName1.text = convertView!!.nomCompletEnf.text.toString()
                                    convertView!!.benefValidChildCarteId1.text = "-  " + convertView!!.carteIdentityEnf.text.toString()
                                    convertView!!.nomCompletEnf.text = null
                                    convertView!!.carteIdentityEnf.text = null
                                    convertView!!.telMailEnf.text = null

                                } else if (nbClick == 2) {

                                    parent2.addView(validateLayout2)
                                    convertView!!.benefValidChildName2.text = convertView!!.nomCompletEnf.text.toString()
                                    convertView!!.benefValidChildCarteId2.text = "-  " + convertView!!.carteIdentityEnf.text.toString()
                                    convertView!!.nomCompletEnf.text = null
                                    convertView!!.carteIdentityEnf.text = null
                                    convertView!!.telMailEnf.text = null

                                } else if (nbClick == 3) {

                                    parent3.addView(validateLayout3)
                                    convertView!!.benefValidChildName3.text = convertView!!.nomCompletEnf.text.toString()
                                    convertView!!.benefValidChildCarteId3.text = "-  " + convertView!!.carteIdentityEnf.text.toString()
                                    convertView!!.nomCompletEnf.text = null
                                    convertView!!.carteIdentityEnf.text = null
                                    convertView!!.telMailEnf.text = null

                                } else if (nbClick == 4) {

                                    parent4.addView(validateLayout4)
                                    convertView!!.benefValidChildName4.text = convertView!!.nomCompletEnf.text.toString()
                                    convertView!!.benefValidChildCarteId4.text = "-  " + convertView!!.carteIdentityEnf.text.toString()
                                    convertView!!.nomCompletEnf.text = null
                                    convertView!!.carteIdentityEnf.text = null
                                    convertView!!.telMailEnf.text = null

                                } else if (nbClick == 5) {

                                    parent5.addView(validateLayout5)
                                    convertView!!.benefValidChildName5.text = convertView!!.nomCompletEnf.text.toString()
                                    convertView!!.benefValidChildCarteId5.text = "-  " + convertView!!.carteIdentityEnf.text.toString()
                                    convertView!!.nomCompletEnf.text = null
                                    convertView!!.carteIdentityEnf.text = null
                                    convertView!!.telMailEnf.text = null
                                }

                            } else {

                                val myLayout = convertView!!.findViewById<LinearLayout>(R.id.myLayoutEnf)
                                val parent: ViewGroup = myLayout.parent as ViewGroup
                                parent.removeView(myLayout)
                                convertView!!.sendInviteEnf.visibility = View.INVISIBLE
                                convertView!!.invitTextIdEnf.visibility = View.INVISIBLE
                                Toast.makeText(
                                    context,
                                    "Vous avez atteint le nombres de bénéficiaires enfants pour lesquels vous avez payez",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }
                }
            }

        }

        /*if (headerTitle === "Adultes") {
            convertView!!.invitTextId.text = "Inviter des adultes "

        } else {
            convertView!!.invitTextId.text = "Inviter des enfants "
        }*/
        return convertView!!
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return false
    }

    override fun getChildType(groupPosition: Int, childPosition: Int): Int {
        when(groupPosition){
            0 -> {
                when(childPosition){
                    0 -> {
                        return 0
                    }
                    1 -> {
                        return 1
                    }
                }
            }
            1 -> {
                when(childPosition){
                    0 -> {
                        return 1
                    }
                    1 -> {
                        return 2
                    }
                }
            }
        }
        return 2
    }

    override fun getChildTypeCount(): Int {
        return 2
    }

    override fun getGroupType(groupPosition: Int): Int {
        when(groupPosition){
            0 -> {
                return 0
            }
            1 -> {
                return 1
            }
        }
        return groupPosition
    }

    override fun getGroupTypeCount(): Int {
        return 2
    }
























    /*      */
}
