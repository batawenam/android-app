package com.example.arsenandroid.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class cities(var towns : String) {
    class Deserializer : ResponseDeserializable<cities> {
        override fun deserialize(content: String): cities? {
            return Gson().fromJson(content, cities::class.java)
        }
    }
}