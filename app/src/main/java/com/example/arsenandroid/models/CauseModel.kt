package com.example.arsenandroid.models

data class CauseModel(var id : String, var title : String,
                      var shortDesc: String, var text: String,
                      var year : Int, var nbrVote : Int, var voted: Boolean) {
}