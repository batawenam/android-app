package com.example.arsenandroid.models

data class ConfirmToken (var token : String, var phone : String, var country_code: String)