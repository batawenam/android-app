package com.example.arsenandroid.models

data class SignUpModel (
        var password: String,
        var firstName: String,
        var lastName: String,
        var gender: String,
        var birthday : String,
        var residencePlace: String,
        var profession: String)