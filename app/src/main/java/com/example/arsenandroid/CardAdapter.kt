package com.example.arsenandroid

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.arsenandroid.view.CardItem
import java.util.ArrayList

class CardAdapter(context: Context, resource: Int) : ArrayAdapter<Int>(context, resource) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val image_view: ImageView = convertView!!.findViewById(R.id.cardImage)
        image_view.setImageResource(getItem(position))
        return convertView
    }
}
