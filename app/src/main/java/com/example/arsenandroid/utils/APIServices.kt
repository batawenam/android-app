package com.example.arsenandroid.utils


import com.example.arsenandroid.models.*
import retrofit2.Call
import retrofit2.http.*


interface APIServices {

    @PUT("/api/v1/entrance/login")
    @FormUrlEncoded
    fun logUser(
        @Field("identifiant") identifiant: String,
        @Field("password") password: String
    ): Call<Any>

    @PUT("/api/v1/entrance/signup/ci-cheking")
    @FormUrlEncoded
    fun sendCI(
        @Field("ci") ci: String
    ): Call<Cni>

    @PUT("/api/v1/entrance/signup/ci-cheking")
    @FormUrlEncoded
    fun verifyAccount(
        @Field("ci") ci: String
    ): Call<Any>

    @PUT("/api/v1/get-causes")
    @FormUrlEncoded
    fun getCauses(
        @Field("userId") userId : String,
        @Field("year") year : Int
    ): Call<List<CauseModel>>


    @PUT("/api/v1/vote-cause")
    @FormUrlEncoded
    fun voteCause(
        @Field("userId") userId : String,
        @Field("idCause") idCause : String
    ): Call<List<CauseModel>>

    @POST("/api/v1/entrance/signup/request-phone-verification")
    @FormUrlEncoded
    fun phoneTokenRequest(
        @Field("country_code") country_code: Int,
        @Field("phone") phone: String,
        @Field("ci") ci: String
    ): Call<Any>

    @POST("/api/v1/entrance/signup/request-email-verification")
    @FormUrlEncoded
    fun  mailTokenRequest(
        @Field("isBusiness") isBusiness: String,
        @Field("emailAddress") emailAddress: String,
        @Field("ci") ci: String
    ): Call<Any>

    @PUT("/api/v1/entrance/confirm-phone")
    @FormUrlEncoded
    fun phoneVerifyToken(
        @Field("token") token: String,
        @Field("phone") phone: String,
        @Field("country_code") country_code: Int
    ): Call<Any>


    @PUT("/api/v1/entrance/confirm-phone")
    @FormUrlEncoded
    fun mailVerifyToken(
        @Field("token") token: String,
        @Field("phone") phone: String,
        @Field("country_code") country_code: Int
    ): Call<Any>


    @POST("/api/v1/entrance/signup/particulier")
    @FormUrlEncoded
    fun sendDatas(
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("password") password: String,
        @Field("gender") gender: String,
        @Field("birthday") birthday: String,
        @Field("residencePlace") residencePlace: String,
        @Field("profession") profession: String,
        @Field("entreprise") entreprise: String,
        @Field("accountId") accountId: String
    ): Call<Any>

    //chat requests


}