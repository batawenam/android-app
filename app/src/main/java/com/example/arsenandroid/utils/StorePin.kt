package com.example.arsenandroid.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class StorePin {

    fun savePin(codePin : String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_PIN, codePin)
        prefEditor.apply()
        return true
    }

    fun getCodePin(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_PIN, null)
    }

    fun saveStatut(state : String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_PIN_STATE, state)
        prefEditor.apply()
        return true
    }

    fun getState(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_PIN_STATE, null)
    }
}