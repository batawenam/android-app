package com.example.arsenandroid.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.annotation.NonNull
import com.example.arsenandroid.models.CauseModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StoreCauses {


    fun saveList(list: List<CauseModel>, context: Context) : Boolean {
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        val gson = Gson()
        val json = gson.toJson(list)
        prefEditor.putString(PrefConstants().CAUSES, json)
        prefEditor.apply()
        return true
    }


    fun getlist(context: Context){
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = preferences.getString(PrefConstants().CAUSES, null)
        val type = object : TypeToken<List<CauseModel>>() {
        }.type
        var causes: List<CauseModel> = gson.fromJson(json, type)
    }
}