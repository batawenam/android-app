package com.example.arsenandroid.utils


class APIUtils{
    private fun ApiUtils() {}

    val BASE_URL = "http://api.helloarsen.com/"

    fun getAPIService(): APIServices {
        return RetrofitClient().getClient(BASE_URL)!!.create(APIServices::class.java)
    }
}
