package com.example.arsenandroid.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.provider.Settings
import com.example.arsenandroid.models.UserModel


class SaveLogged {

    fun saveIdentity(identifiant : String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_ID, identifiant)
        prefEditor.apply()
        return true
    }

    fun getIdentity(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_ID, null)
    }

    fun savePassword(password : String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_PASSWORD, password)
        prefEditor.apply()
        return true
    }

    fun getPassword(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_PASSWORD, null)
    }



    fun saveFirstName(firstName: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_FIRSTNAME, firstName)
        prefEditor.apply()
        return true
    }

    fun getFirstName(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_FIRSTNAME, null)
    }

    fun savelastName(lastName: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_LASTNAME, lastName)
        prefEditor.apply()
        return true
    }

    fun getlastName(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_LASTNAME, null)
    }

    fun saveGender(gender: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_GENDER, gender)
        prefEditor.apply()
        return true
    }

    fun getGender(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_GENDER, null)
    }

    fun savePhone(phone: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_PHONE, phone)
        prefEditor.apply()
        return true
    }

    fun getPhone(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_PHONE, null)
    }

    fun saveProfession(profession: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_PROFESSION, profession)
        prefEditor.apply()
        return true
    }

    fun getProfession(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_PROFESSION, null)
    }

    fun saveUserId(userId: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_IDUSER, userId)
        prefEditor.apply()
        return true
    }

    fun getUserId(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_IDUSER, null)
    }

    fun saveCardId(cardId: String, context: Context) : Boolean{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val prefEditor : SharedPreferences.Editor = preferences.edit()
        prefEditor.putString(PrefConstants().KEY_CARDID, cardId)
        prefEditor.apply()
        return true
    }

    fun getCardId(context: Context) : String?{
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(PrefConstants().KEY_CARDID, null)
    }


}