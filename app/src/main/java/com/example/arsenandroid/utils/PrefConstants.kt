package com.example.arsenandroid.utils

class PrefConstants {
    val KEY_ID : String = "identifiant"
    val KEY_PASSWORD : String = "password"
    val KEY_FIRSTNAME : String = "firstname"
    val KEY_LASTNAME : String = "lastname"
    val KEY_GENDER : String = "gender"
    val KEY_RESIDENCE : String = "residence"
    val KEY_PROFESSION : String = "profession"
    val KEY_PHONE : String = "phone"
    val KEY_IDUSER : String = "userid"
    val KEY_CARDID : String = "cardid"
    val KEY_PIN : String = "CODE_PIN"
    val KEY_PIN_STATE : String = "PIN_STATE"
    val CAUSES : String = ""
}