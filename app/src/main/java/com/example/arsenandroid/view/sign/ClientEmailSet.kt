package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.models.verifyDatas
import com.example.arsenandroid.utils.APIUtils
import com.github.kittinunf.fuel.httpPost
import com.google.gson.Gson
import kotlinx.android.synthetic.main.email_telephone.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClientEmailSet: AppCompatActivity() {

    val apiService = APIUtils().getAPIService()
    val cuntry_code = 229

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.email_telephone)

        mailLayout.visibility = View.VISIBLE
        errorMail.visibility = View.INVISIBLE

        val adressMail = adresse.text

        skipButton.setOnClickListener {
            onBackPressed()
        }

        suivantButton.setOnClickListener {
            if (radioEmail.isChecked){
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(adressMail).matches() && !android.util.Patterns.PHONE
                        .matcher(adressMail).matches()){
                    errorMail.visibility = View.VISIBLE
                }
                else{
                    mailRequest()
                }
            }

            if (radioTel.isChecked){
                phoneRequest()
            }

        }

        radioEmail.setOnClickListener{
            mailLayout.visibility = View.VISIBLE
            telLayout.visibility = View.INVISIBLE

            adresse.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(adressMail).matches() && !android.util.Patterns.PHONE
                            .matcher(adressMail).matches()){
                        adresse.error = "Email incorrect"
                        errorMail.visibility = View.VISIBLE
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(adressMail).matches() && !android.util.Patterns.PHONE
                            .matcher(adressMail).matches()){
                        adresse.error = "Email incorrect"
                        errorMail.visibility = View.VISIBLE
                    }
                }

            })
        }

        radioTel.setOnClickListener{
            telLayout.visibility = View.VISIBLE
            mailLayout.visibility = View.INVISIBLE

        }

    }




    private fun mailRequest() {

        val ci = intent.getStringExtra("CNI")
        val isBusiness = "false"

            val email = adresse.text.toString()
            apiService.mailTokenRequest(isBusiness, email, ci).enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                    Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {

                    if (response.code() == 200 || response.code() == 500) {
                        val jsonObject = JSONObject(response.body() as Map<*, *>)
                        val intent = Intent(this@ClientEmailSet, ConfirmationCode::class.java)
                        intent.putExtra("Email", email)
                        startActivity(intent)
                        accountid = jsonObject.getString("accountId")

                    } else {
                        Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                    }

                }
            })
    }

    fun phoneRequest(){
        val ci = intent.getStringExtra("CNI")
        val phone = telNumber.text.toString()

        apiService.phoneTokenRequest(cuntry_code, phone, ci).enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {

                if (response.code() == 200) {
                    val jsonObject = JSONObject(response.body() as Map<*, *>)
                    val intent = Intent(this@ClientEmailSet, ConfirmationCode::class.java)
                    intent.putExtra("Phone", phone)
                    intent.putExtra("COUNTRY_CODE", "229")
                    startActivity(intent)
                    accountid = jsonObject.getString("accountId")


                } else {
                    Toast.makeText(applicationContext, "Erreur, ${response.code()}", Toast.LENGTH_SHORT).show()
                }

            }
        })
    }

    companion object {
        var accountid = ""
    }

}