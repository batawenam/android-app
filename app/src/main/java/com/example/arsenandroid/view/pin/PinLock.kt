package com.example.arsenandroid.view.pin

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.utils.StorePin
import com.example.arsenandroid.view.menu.Navigation
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.keyboard_view.*

class PinLock : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main1)

        val stringBuilder = StringBuilder()

        editPin1.addTextChangedListener( object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (stringBuilder.isEmpty()){
                    editPin1.requestFocus()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (stringBuilder.length == 1){
                    stringBuilder.deleteCharAt(0)
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((stringBuilder.isEmpty()) && (editPin1.length() == 1)){
                    stringBuilder.append(s)
                    editPin1.clearFocus()
                    editPin2.requestFocus()
                    editPin2.isCursorVisible = true
                }

            }

        })

        editPin2.addTextChangedListener( object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (stringBuilder.isEmpty()){
                    editPin2.requestFocus()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (stringBuilder.length == 1){
                    stringBuilder.deleteCharAt(0)
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((stringBuilder.isEmpty()) && (editPin2.length() == 1)){
                    stringBuilder.append(s)
                    editPin2.clearFocus()
                    editPin3.requestFocus()
                    editPin3.isCursorVisible = true
                }
            }

        })

        editPin3.addTextChangedListener( object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (stringBuilder.isEmpty()){
                    editPin3.requestFocus()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (stringBuilder.length == 1){
                    stringBuilder.deleteCharAt(0)
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((stringBuilder.isEmpty()) && (editPin1.length() == 1)){
                    stringBuilder.append(s)
                    editPin3.clearFocus()
                    editPin4.requestFocus()
                    editPin4.isCursorVisible = true
                }
            }

        })

        if (StorePin().getCodePin(this@PinLock) == null || StorePin().getCodePin(this@PinLock)!!.isEmpty()) {
            pinAction.text = "Définir un code secret"

            editPin4.addTextChangedListener( object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {
                    savePin()
                    StorePin().saveStatut("UNCONFIRMED", this@PinLock)
                    finish()
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

            })

        }

        editPin1.showSoftInputOnFocus = false
        editPin1.transformationMethod = PasswordTransformationMethod.getInstance()
        editPin1.isFocusable = false

        editPin2.showSoftInputOnFocus = false
        editPin2.transformationMethod = PasswordTransformationMethod.getInstance()
        editPin2.isFocusable = false


        editPin3.showSoftInputOnFocus = false
        editPin3.transformationMethod = PasswordTransformationMethod.getInstance()
        editPin3.isFocusable = false


        editPin4.showSoftInputOnFocus = false
        editPin4.transformationMethod = PasswordTransformationMethod.getInstance()
        editPin4.isFocusable = false


        keyboardClick()


    }

    private fun keyboardClick(){
        key1.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_1)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_1)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_1)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_1)
            }
        }

        key2.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_2)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_2)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_2)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_2)
            }
        }

        key3.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_3)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_3)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_3)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_3)
            }
        }

        key4.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_4)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_4)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_4)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_4)
            }
        }

        key5.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_5)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_5)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_5)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_5)
            }
        }

        key6.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_6)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_6)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_6)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_6)
            }
        }

        key7.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_7)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_7)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_7)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_7)
            }
        }

        key8.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_8)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_8)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_8)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_8)
            }
        }

        key9.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_9)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_9)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_9)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_9)
            }
        }

        key0.setOnClickListener {

            when {
                editPin1.text!!.isEmpty() -> editPin1.setText(R.string.key_0)
                editPin2.text!!.isEmpty() -> editPin2.setText(R.string.key_0)
                editPin3.text!!.isEmpty() -> editPin3.setText(R.string.key_0)
                editPin4.text!!.isEmpty() -> editPin4.setText(R.string.key_0)
            }
        }

        key_remove.setOnClickListener {

            if (editPin4.text!!.isNotEmpty()){
                editPin4.text!!.clear()
            }else if (editPin3.text!!.isNotEmpty()){
                editPin3.text!!.clear()
            }else if (editPin2.text!!.isNotEmpty()){
                editPin2.text!!.clear()
            }else if (editPin1.text!!.isNotEmpty()){
                editPin1.text!!.clear()
            }
        }
    }

    private fun savePin(){
        val pinValue = editPin1.text.toString()+editPin2.text.toString()+editPin3.text.toString()+editPin4.text.toString()
        if (editPin1.text!!.isNotEmpty() && editPin2.text!!.isNotEmpty()
            && editPin3.text!!.isNotEmpty() && editPin4.text!!.isNotEmpty()){
            StorePin().savePin(pinValue, this@PinLock)
            startActivity(Intent(this, Confirmed::class.java))
        }

    }

    fun clearEdit(){
        val edits : ArrayList<EditText> = ArrayList()
        edits.add(editPin1)
        edits.add(editPin2)
        edits.add(editPin3)
        edits.add(editPin4)
    }



}