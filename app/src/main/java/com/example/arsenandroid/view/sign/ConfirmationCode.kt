package com.example.arsenandroid.view.sign

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import com.example.arsenandroid.R
import com.example.arsenandroid.models.ConfirmToken
import com.example.arsenandroid.utils.APIUtils
import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.secretcode_screen.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConfirmationCode : AppCompatActivity(){

    val apiService = APIUtils().getAPIService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.secretcode_screen)

        var sb = StringBuilder()

        case1.addTextChangedListener(object: TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((sb.length == 0) && (case1.length() == 1)){
                    sb.append(s)
                    case1.clearFocus()
                    case2.requestFocus()
                    case2.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (sb.length == 1){
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0){
                    case1.requestFocus()
                }
            }

        })

        case2.addTextChangedListener(object: TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((sb.length == 0) && (case2.length() == 1)){
                    sb.append(s)
                    case2.clearFocus()
                    case3.requestFocus()
                    case3.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (sb.length == 1){
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0){
                    case2.requestFocus()
                }
            }

        })

        case3.addTextChangedListener(object: TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((sb.length == 0) && (case3.length() == 1)){
                    sb.append(s)
                    case3.clearFocus()
                    case4.requestFocus()
                    case4.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (sb.length == 1){
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0){
                    case3.requestFocus()
                }
            }

        })

        case4.addTextChangedListener(object: TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((sb.length == 0) && (case4.length() == 1)){
                    sb.append(s)
                    case4.clearFocus()
                    case5.requestFocus()
                    case5.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (sb.length == 1){
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0){
                    case4.requestFocus()
                }
            }

        })

        case5.addTextChangedListener(object: TextWatcher {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((sb.length == 0) && (case5.length() == 1)){
                    sb.append(s)
                    case5.clearFocus()
                    case6.requestFocus()
                    case6.isCursorVisible = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if (sb.length == 1){
                    sb.deleteCharAt(0)
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (sb.length == 0){
                    case5.requestFocus()
                }
            }

        })

        codeBtn.setOnClickListener {
            verify()
        }
    }

    fun verify(){
        val value1 = case1.text.toString()+case2.text.toString()+case3.text.toString()+case4.text.toString()+case5.text.toString()+case6.text.toString()

        //val value = value1
        val phone = intent.getStringExtra("Phone")
        val cCode = intent.getStringExtra("COUNTRY_CODE")
        val codeRequired = ConfirmToken(value1, phone, cCode)
        val postJson = Gson().toJson(codeRequired)
        Log.d("debug", postJson)

        if (phone != null) {
            apiService.phoneVerifyToken(value1, phone, cCode.toInt()).enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                    Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {

                    if (response.code() == 200 || response.code() == 500) {
                        val jsonObject = JSONObject(response.body() as Map<*, *>)
                        val intent = Intent(this@ConfirmationCode, PasswordSet::class.java)
                        startActivity(intent)

                    } else {
                        Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                    }

                }

            })
        }else{
            apiService.mailVerifyToken(value1, phone, cCode.toInt()).enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                    Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {

                    if (response.code() == 200 || response.code() == 500) {
                        val jsonObject = JSONObject(response.body() as Map<*, *>)
                        val intent = Intent(this@ConfirmationCode, PasswordSet::class.java)
                        startActivity(intent)

                    } else {
                        Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
                    }

                }

            })
        }

    }

}
