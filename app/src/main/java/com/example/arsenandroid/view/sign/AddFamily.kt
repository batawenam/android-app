package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.add_family.*

class AddFamily : AppCompatActivity() {

    companion object {
        var oldNb = 0
        var childNb = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_family)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }
        skipButton.setOnClickListener {
            onBackPressed()
        }

        oldAdd.setOnClickListener {
            oldNb += 1
            oldCounter.text = oldNb.toString()
        }

        oldSubstract.setOnClickListener {
            when {
                oldNb > 0 -> {
                    oldNb -= 1
                    oldCounter.text = oldNb.toString()
                }
            }
        }

        childAdd.setOnClickListener {
            childNb += 1
            childCounter.text = childNb.toString()
        }

        childSubstract.setOnClickListener {
            when {
                childNb > 0 -> {
                    childNb -= 1
                    childCounter.text = childNb.toString()
                }
            }
        }

        continueButton.setOnClickListener {
            var intent = Intent(this, Sommaire::class.java)
            intent.putExtra("adultes", oldNb)
            intent.putExtra("enfants", childNb)
            startActivity(intent)
        }

        skipButton.setOnClickListener {
            onBackPressed()
        }
    }
}
