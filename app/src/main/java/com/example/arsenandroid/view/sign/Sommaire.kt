package com.example.arsenandroid.view.sign

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import co.opensi.kkiapay.uikit.Kkiapay
import com.example.arsenandroid.R
import com.example.arsenandroid.view.menu.Navigation
import kotlinx.android.synthetic.main.sommaire.*
import java.text.DecimalFormat

class Sommaire : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sommaire)

        skipButton.setOnClickListener {
            onBackPressed()
        }

        var adulteNb: TextView = findViewById(R.id.adulteNb)
        var enfantNb: TextView = findViewById(R.id.EnfantNb)
        var intent = intent
        var bundle: Bundle = intent.extras

        if (bundle != null) {
            var nbTotalAdulte: Int = bundle.get("adultes") as Int
            var nbTotalEnfants: Int = bundle.get("enfants") as Int
            adulteNb.text = "($nbTotalAdulte)"
            enfantNb.text = "($nbTotalEnfants)"

            //val formatter = DecimalFormat("#,###,###")
            //formatter.format(totalSolo6Mois)
            val formatter = DecimalFormat("###,###,###")
            if (ChoosePlan.planSelected == 1 && ContratDuration.planDuration == 1) {

                val totalSolo6Mois: Long = ChoosePlan.expressTarif * ChoosePlan.semestre
                prixSolo.text = formatter.format(totalSolo6Mois)
                val totalAdulte6mois: Long = ChoosePlan.expressTarif * ChoosePlan.semestre * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte6mois)
                val totalEnfant6mois: Long = (ChoosePlan.expressTarif * ChoosePlan.semestre * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant6mois)

                ChoosePlan.montantTotal = totalSolo6Mois + totalAdulte6mois + totalEnfant6mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            } else if (ChoosePlan.planSelected == 1 && ContratDuration.planDuration == 2) {

                val totalSolo12Mois: Long = ChoosePlan.expressTarif * ChoosePlan.annee
                prixSolo.text = formatter.format(totalSolo12Mois)
                val totalAdulte12mois: Long = ChoosePlan.expressTarif * ChoosePlan.annee * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte12mois)
                val totalEnfant12mois: Long = (ChoosePlan.expressTarif * ChoosePlan.annee * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant12mois)

                ChoosePlan.montantTotal = totalSolo12Mois + totalAdulte12mois + totalEnfant12mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            } else if (ChoosePlan.planSelected == 2 && ContratDuration.planDuration == 1) {

                val totalSolo6Mois: Long = ChoosePlan.proTarif * ChoosePlan.semestre
                prixSolo.text = formatter.format(totalSolo6Mois)
                val totalAdulte6mois: Long = ChoosePlan.proTarif * ChoosePlan.semestre * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte6mois)
                val totalEnfant6mois: Long = (ChoosePlan.proTarif * ChoosePlan.semestre * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant6mois)

                ChoosePlan.montantTotal = totalSolo6Mois + totalAdulte6mois + totalEnfant6mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            } else if (ChoosePlan.planSelected == 2 && ContratDuration.planDuration == 2) {

                val totalSolo12Mois: Long = ChoosePlan.proTarif * ChoosePlan.annee
                prixSolo.text = formatter.format(totalSolo12Mois)
                val totalAdulte12mois: Long = ChoosePlan.proTarif * ChoosePlan.annee * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte12mois)
                val totalEnfant12mois: Long = (ChoosePlan.proTarif * ChoosePlan.annee * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant12mois)

                ChoosePlan.montantTotal = totalSolo12Mois + totalAdulte12mois + totalEnfant12mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            } else if (ChoosePlan.planSelected == 3 && ContratDuration.planDuration == 1) {

                val totalSolo6Mois: Long = ChoosePlan.premiumTarif * ChoosePlan.semestre
                prixSolo.text = formatter.format(totalSolo6Mois)
                val totalAdulte6mois: Long = ChoosePlan.premiumTarif * ChoosePlan.semestre * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte6mois)
                val totalEnfant6mois: Long = (ChoosePlan.premiumTarif * ChoosePlan.semestre * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant6mois)

                ChoosePlan.montantTotal = totalSolo6Mois + totalAdulte6mois + totalEnfant6mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            } else if (ChoosePlan.planSelected == 3 && ContratDuration.planDuration == 2) {

                val totalSolo12Mois: Long = ChoosePlan.premiumTarif * ChoosePlan.annee
                prixSolo.text = formatter.format(totalSolo12Mois)
                val totalAdulte12mois: Long = ChoosePlan.premiumTarif * ChoosePlan.annee * nbTotalAdulte
                prixAdulte.text = formatter.format(totalAdulte12mois)
                val totalEnfant12mois: Long = (ChoosePlan.premiumTarif * ChoosePlan.annee * nbTotalEnfants) / 2
                prixEnfant.text = formatter.format(totalEnfant12mois)

                ChoosePlan.montantTotal = totalSolo12Mois + totalAdulte12mois + totalEnfant12mois
                totalAPayer.text = formatter.format(ChoosePlan.montantTotal) + " F CFA"

            }
        }
        Kkiapay.get().setListener { status, transactionId ->
            // ecoutez la fin du paiement ( status contient les différents status possibles )
            Toast.makeText(this, "Transaction: ${status.name} -> $transactionId", Toast.LENGTH_LONG).show()
        }


        continueButton.setOnClickListener {
            Kkiapay.get().requestPayment(this, ChoosePlan.montantTotal.toString(), "Paiement de services", "Jhon Doe")
        }

    }
}
