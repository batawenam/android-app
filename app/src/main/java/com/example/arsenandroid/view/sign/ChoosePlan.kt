package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.abonnement_plan.*

class ChoosePlan : AppCompatActivity() {

    companion object {
        var planSelected: Int = 0
        var expressTarif: Long = 5600
        var proTarif: Long = 7200
        var premiumTarif: Long = 9999
        var semestre: Int = 6
        var annee: Int = (12 - 1)
        var montantTotal: Long = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.abonnement_plan)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }
        skipButton.setOnClickListener {
            onBackPressed()
        }

        planExpress.setOnClickListener {
            planExpress.isClickable = true
            selectExpress(R.drawable.neutral_stroke, R.drawable.border_green)
        }

        planPro.setOnClickListener {
            planPro.isClickable = true
            selectPro(R.drawable.neutral_stroke, R.drawable.border_blue)
        }

        planPremium.setOnClickListener {
            planPremium.isClickable = true
            selectPremium(R.drawable.neutral_stroke, R.drawable.premium_border)
        }

        chooseExpress.setOnClickListener {
            planExpress.isClickable = true
            selectExpress(R.drawable.neutral_stroke, R.drawable.border_green)
            startActivity(Intent(this, ContratDuration::class.java))
            planSelected = 1
        }

        choosePro.setOnClickListener {
            planPro.isClickable = true
            selectPro(R.drawable.neutral_stroke, R.drawable.border_blue)
            startActivity(Intent(this, ContratDuration::class.java))
            planSelected = 2
        }

        choosePremium.setOnClickListener {
            planPremium.isClickable = true
            selectPremium(R.drawable.neutral_stroke, R.drawable.premium_border)
            startActivity(Intent(this, ContratDuration::class.java))
            planSelected = 3
        }

        skipButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun selectPremium(firstColor: Int, secondColor: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            planExpress.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
            planPro.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
            planPremium.setBackgroundDrawable(ContextCompat.getDrawable(this, secondColor))
        } else {
            planExpress.background = ContextCompat.getDrawable(this, firstColor)
            planPro.background = ContextCompat.getDrawable(this, firstColor)
            planPremium.background = ContextCompat.getDrawable(this, secondColor)
        }
    }

    private fun selectPro(firstColor: Int, secondColor: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            planExpress.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
            planPro.setBackgroundDrawable(ContextCompat.getDrawable(this, secondColor))
            planPremium.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
        } else {
            planExpress.background = ContextCompat.getDrawable(this, firstColor)
            planPro.background = ContextCompat.getDrawable(this, secondColor)
            planPremium.background = ContextCompat.getDrawable(this, firstColor)
        }
    }

    private fun selectExpress(firstColor: Int, secondColor: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            planExpress.setBackgroundDrawable(ContextCompat.getDrawable(this, secondColor))
            planPro.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
            planPremium.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
        } else {
            planExpress.background = ContextCompat.getDrawable(this, secondColor)
            planPro.background = ContextCompat.getDrawable(this, firstColor)
            planPremium.background = ContextCompat.getDrawable(this, firstColor)
        }
    }
}
