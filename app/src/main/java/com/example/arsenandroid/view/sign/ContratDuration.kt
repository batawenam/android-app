package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.contrat_duree.*

class ContratDuration : AppCompatActivity() {

    companion object {
        var planDuration: Int = 0
    }

    var clicked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contrat_duree)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }

        skipButton.setOnClickListener {
            onBackPressed()
        }

        chooseSemestre.setOnClickListener {
            selectedLayout(R.drawable.border_blue, R.drawable.neutral_stroke)
            semestreDot.setImageResource(R.drawable.ic_contrat_selected)
            yearDot.setImageResource(R.drawable.ic_unselected_contrat)
            clicked = true
            planDuration = 1
        }

        chooseYear.setOnClickListener {
            selectedLayout(R.drawable.neutral_stroke, R.drawable.border_blue)
            yearDot.setImageResource(R.drawable.ic_contrat_selected)
            semestreDot.setImageResource(R.drawable.ic_unselected_contrat)
            clicked = true
            planDuration = 2
        }

        continueButton.setOnClickListener {
            if (clicked) {
                startActivity(Intent(this, AddFamily::class.java))
            } else {
                var toast: Toast =
                    Toast.makeText(this, "Veuillez choisir une durée s'il vous plait", Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.TOP, Gravity.CENTER_HORIZONTAL, 40)
                toast.show()
            }
        }
        skipButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun selectedLayout(firstColor: Int, secondColor: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            chooseSemestre.setBackgroundDrawable(ContextCompat.getDrawable(this, firstColor))
            chooseYear.setBackgroundDrawable(ContextCompat.getDrawable(this, secondColor))
        } else {
            chooseSemestre.background = ContextCompat.getDrawable(this, firstColor)
            chooseYear.background = ContextCompat.getDrawable(this, secondColor)
        }
    }
}
