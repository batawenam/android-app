package com.example.arsenandroid.view


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.arsenandroid.R
import com.example.arsenandroid.models.CauseModel
import com.example.arsenandroid.utils.APIUtils
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.utils.StoreCauses
import kotlinx.android.synthetic.main.socialcause_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CauseAdapter(var causes : List<CauseModel>, var  context: Context) : RecyclerView.Adapter<CauseAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.socialcause_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = causes.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindItems(causes[position], context)
    }


    class MyViewHolder(val itView: View) : RecyclerView.ViewHolder(itView){

        val apiService = APIUtils().getAPIService()

        fun bindItems(cause: CauseModel, context: Context){

            val title = itView.findViewById<TextView>(R.id.causeTitle)
            val subTitle = itView.findViewById<TextView>(R.id.causeSubTitle)
            val causeTxt = itView.findViewById<TextView>(R.id.causeTxt)
            val nbrVote = itView.findViewById<TextView>(R.id.voteNum)

            title.text = cause.title
            subTitle.text = cause.shortDesc
            causeTxt.text = cause.text
            nbrVote.text = cause.nbrVote.toString()
            val id = cause.id
            val voteState = cause.voted
            val uId = SaveLogged().getUserId(context)!!
            //val allCauses : CauseModel? = null

            if (voteState){
                itView.voteArea.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
                itView.arrow.setImageResource(R.drawable.ic_arrow_drop_up)
                itView.voteNum.setTextColor(ContextCompat.getColor(context, R.color.color_white))
            }

            itView.voteArea.setOnClickListener {
                itView.voteArea.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
                itView.arrow.setImageResource(R.drawable.ic_arrow_drop_up)
                itView.voteNum.setTextColor(ContextCompat.getColor(context, R.color.color_white))

               apiService.voteCause(uId, id).enqueue(object : Callback<List<CauseModel>> {

                    override fun onFailure(call: Call<List<CauseModel>>, t: Throwable) {
                        Toast.makeText(context, "Erreur, veuillez réessayer ${t.cause}", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<List<CauseModel>>, response: Response<List<CauseModel>>) {
                        if (response.isSuccessful) {
                            val causeList = response.body()
                            StoreCauses().saveList(causeList!!, context)
                        }
                    }
                })
            }

            itView.voteArea.isEnabled
        }


    }
}