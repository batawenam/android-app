package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.profil_choice.*
import android.text.method.LinkMovementMethod


class ClientStatus : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profil_choice)

        skipButton.setOnClickListener {
            onBackPressed()
        }

        suivantButton.setOnClickListener {
            startActivity(Intent(applicationContext, ClientIdCard::class.java))
        }

        arsenLink.movementMethod = LinkMovementMethod.getInstance()
    }

}