package com.example.arsenandroid.view.garanties

import android.Manifest
import android.app.Activity
import android.content.ClipData
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.example.arsenandroid.R
import com.example.arsenandroid.view.CustomPhotoGalleryActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.garanties_details.*
import kotlinx.android.synthetic.main.import_facture_dialog.*
import kotlinx.android.synthetic.main.success_import_camera.*
import kotlinx.android.synthetic.main.sucess_importing_dialog.*
import java.util.*

class GarantiesDetails : AppCompatActivity() {

    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"

    }

    //private lateinit var lnrImages: LinearLayout
    private lateinit var imagesPathList: ArrayList<String>
    private lateinit var yourbitmap: Bitmap
    private val resized: Bitmap? = null
    private val PICK_IMAGE_MULTIPLE = 1

    private var GALLERY = 1
    private val CAMERA = 1001
    private val GALLERY_SECOND = 2
    private var PDF = 1212
    var prise = false

    var image_uri: Uri? = null

    lateinit var garantImg: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.garanties_details)

        /*overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }

        //lnrImages = findViewById(R.id.lnrImages) as LinearLayout

        val title = findViewById<TextView>(R.id.itemText)
        val text = findViewById<TextView>(R.id.itemSubtitle)
        garantImg = findViewById(R.id.detailsItemImg)
        val intent = intent
        val b: Bundle = intent.extras
        if (b != null) {
            val j: String = b.get("title") as String
            val i: String = b.get("text") as String
            val resId: Int = b.get("resId") as Int
            garantImg.setImageResource(resId)
            title.text = j
            text.text = i
        }
        //Dialogue pour afficher le choix de sélection(Import photo, prise de photo avec camera ou import pdf)
        demandRembourButton.setOnClickListener {
            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.import_facture_dialog, null)
            dialog.setContentView(view)
            dialog.show()

            //Clique pour importer le pdf avec caméra
            dialog.import_facture_pdf.setOnClickListener {
                takePdfFromPhone()
            }

            //Clique pour importer la photo avec caméra
            dialog.import_facture_photo.setOnClickListener {
                choosePhotoFromGallery()
            }

            //Clique pour prendre la photo avec caméra
            dialog.take_facture_photo.setOnClickListener {
                takePhotoFromCamera()
            }
        }


//        img1.setOnClickListener {
//            val intent = Intent()
//            intent.setType("image/*")
//            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
//            intent.setAction(Intent.ACTION_GET_CONTENT)
//            startActivityForResult(Intent.createChooser(intent, "Selectionner plusieurs images"), GALLERY)
//        }

    }

    //Fonction pour importer le pdf
    private fun takePdfFromPhone() {
        val intent = Intent()
        intent.setAction(Intent.ACTION_GET_CONTENT)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.setType("application/pdf")
        startActivityForResult(intent, PDF)
    }

    //Fonction pour importer la photo
    private fun choosePhotoFromGallery() {
        val intent = Intent(this, CustomPhotoGalleryActivity::class.java)
        startActivityForResult(intent, PICK_IMAGE_MULTIPLE)
    }

    //Fonction pour prendre la photo
    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
        GALLERY += 1
    }

    //Résultat après les prises
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_MULTIPLE) { // Si la photo est importer depuis le téléphone
            val contentURI = data!!.data

            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.sucess_importing_dialog, null)
            dialog.setContentView(view)
            dialog.show()

            imagesPathList = ArrayList()
            val imagesPath =
                data.getStringExtra("data").split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            try {
                dialog.lnrImages.removeAllViews()
            } catch (e: Throwable) {
                e.printStackTrace()
            }

            for (i in imagesPath.indices) {
                imagesPathList.add(imagesPath[i])
                yourbitmap = BitmapFactory.decodeFile(imagesPath[i])
                val imageView = ImageView(this)
                imageView.setImageBitmap(yourbitmap)
                imageView.adjustViewBounds = true
                imageView.maxHeight = 150
                imageView.maxWidth = 150
                imageView.minimumHeight = 150
                imageView.minimumWidth = 150
                imageView.setPadding(10,0,0,0)
                dialog.lnrImages.addView(imageView)
            }
            Toast.makeText(this, "Images enregistrées!", Toast.LENGTH_SHORT).show()

        } else if (requestCode == CAMERA) {// Resultat à la prise de photo avec la camera

            if (GALLERY < 3){
                var cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA)
                GALLERY++
            }
            /*val thumbnail = data!!.extras!!.get("data") as Bitmap
            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.success_import_camera, null)
            //dialog de recupération
            dialog.setContentView(view)
            dialog.show()
            dialog.firstImage!!.setImageBitmap(thumbnail)
            dialog.firstImage.maxHeight = 150
            dialog.firstImage.maxWidth = 150
            //saveImage(thumbnail)
            dialog.imgLayout2.setOnClickListener {
                takePhotoFromCamera()
                if (GALLERY > 2 ){
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                }

            }
            Toast.makeText(this, "Image enregistrée!", Toast.LENGTH_SHORT).show()
*/
        } else if (requestCode == PDF) { // Importer un pdf---- resultat
            val uri = data!!.data
            val fileName: String = getFileName(this, uri)// le nom du pdf
            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.success_import_camera, null)
            dialog.setContentView(view)
            dialog.show()
            /*dialog.pdfImg.setImageResource(R.drawable.import_file)
            dialog.imgLayout2.visibility = View.INVISIBLE
            dialog.pdfTitle.text = fileName// afficher le nom du pdf*/
            Toast.makeText(this, "Pdf enregistré!", Toast.LENGTH_SHORT).show()

        } else if (requestCode == GALLERY) {// code non achevé--- prise de la seconde photo

            val clipData: ClipData = data!!.clipData
            if (clipData == null){
                img1.setImageURI(clipData.getItemAt(0).uri)
                img1.setImageURI(clipData.getItemAt(1).uri)

                for (i in 0..clipData.getItemCount()){
                    val item: ClipData.Item = clipData.getItemAt(i)
                    val uri: Uri = item.uri
                    Log.e("Mes images", uri.toString())
                }
            }
        }
    }

    //Fonction permettant d'obtenir le nom du pdf
    fun getFileName(context: Context, uri: Uri): String {
        val returnCursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
        val nameIndex: Int = returnCursor!!.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        val sizeIndex: Int = returnCursor!!.getColumnIndex(OpenableColumns.SIZE)
        if (uri != null && context != null) {
            if (returnCursor != null) {
                returnCursor.moveToFirst()
                if (nameIndex >= 0 && sizeIndex >= 0) {
                    val isValidFile = checkValid(returnCursor.getString(nameIndex))
                    if (!isValidFile)
                        return returnCursor.getString(nameIndex)
                }
            }
        }
        return returnCursor!!.getString(nameIndex)// retour sur le nom
    }

    //Vérifier si c'est vraiment un fichier pdf
    fun checkValid(filePath: String): Boolean {
        val filePathInLOwerCase = filePath.toLowerCase()
        if (filePathInLOwerCase.endsWith(".pdf")) {
            return true
        }
        return false
    }































    // Fonction de sauvegarde des images prises sur la memoire du telephone

    /*/*if (data != null) {
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                //val path = saveImage(bitmap)
                try {
                    Toast.makeText(this, "Image prise!", Toast.LENGTH_SHORT).show()
                    //Afficher le dialogue de recupération de la photo
                    val dialog = BottomSheetDialog(this)
                    val view = layoutInflater.inflate(R.layout.sucess_importing_dialog, null)
                    dialog.setContentView(view)
                    dialog.show()
                    *//*dialog.firstImage!!.setImageBitmap(bitmap)// Afficher ca sur la premier image du dialog
                    //Clique pour ajouter une photo
                    dialog.secondImage.setOnClickListener {
                        other()
                        prise = true
                        dialog.firstImage!!.setImageBitmap(bitmap)
                    }*//*
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Echoué!", Toast.LENGTH_SHORT).show()
                }
            }*/*/

    /*
    val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)

    private fun saveImage(myBitmap: Bitmap?): String {
        val bytes = ByteArrayOutputStream()
        myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance().timeInMillis).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this, arrayOf(f.path), arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::---" + f.absolutePath)
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }*/
    //Seconde methode pour prendre les photos avce la camera

    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
        if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
        || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
            val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            requestPermissions(permission, PERMISSION_REQUEST)
        } else {
            takePhotoFromGallery()
        }
    } else {
        takePhotoFromGallery()
    }*/
    /*val values = ContentValues()
values.put(MediaStore.Images.Media.TITLE, "Nouvelle photo")
values.put(MediaStore.Images.Media.DESCRIPTION, "De le camera")
image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
intent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
startActivityForResult(intent, CAMERA)*/

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
if (resultCode == Activity.RESULT_OK){
image_view.setImageURI(image_uri)
}
}*/
    /*override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_REQUEST -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    takePhotoFromGallery()
                } else {
                    Toast.makeText(this, "Permission refusée", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }*/

/*if (data != null) {
                val contentURI = data!!.data
                val contentURI2 = data!!.data
                //val clipData = data.clipData
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val bitmap2 = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI2)
                    val path = saveImage(bitmap)
                    val path2 = saveImage(bitmap)
                    Toast.makeText(this, "Image prise!", Toast.LENGTH_SHORT).show()
                    val dialog = BottomSheetDialog(this)
                    val view = layoutInflater.inflate(R.layout.sucess_importing_dialog, null)
                    dialog.setContentView(view)
                    dialog.show()
                    dialog.firstImage!!.setImageBitmap(bitmap)
                    dialog.secondImage.setImageBitmap(bitmap2)
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Echoué!", Toast.LENGTH_SHORT).show()
                }
            }*/
}
