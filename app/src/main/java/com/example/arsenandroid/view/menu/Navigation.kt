package com.example.arsenandroid.view.menu

import android.os.Bundle
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.arsenandroid.R
import com.example.arsenandroid.models.CauseModel
import com.example.arsenandroid.utils.APIUtils
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.utils.StoreCauses
import com.example.arsenandroid.view.fragments.AccountFragment
import com.example.arsenandroid.view.fragments.CoachFragment
import com.example.arsenandroid.view.fragments.GarantsFragment
import com.example.arsenandroid.view.fragments.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class Navigation : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val homeFragment = HomeFragment.newInstance()
                openFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_garant -> {
                val garantsFragment = GarantsFragment.newInstance()
                openFragment(garantsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_coach -> {
                val coachFragment = CoachFragment.newInstance()
                openFragment(coachFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_account -> {
                val accountFragment = AccountFragment.newInstance()
                openFragment(accountFragment)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.navigation_menu)

        var bottomNav: BottomNavigationView = findViewById(R.id.navigation)

        bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        //bottomNav.selectedItemId = R.id.navigation_home
        supportFragmentManager.beginTransaction().replace(R.id.frame_container,
            HomeFragment()
        ).commit()

    }
}
