package com.example.arsenandroid.view.lock

import android.app.Activity
import android.content.ClipboardManager
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import com.example.arsenandroid.FingerPrintDialog
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.activity_main1.*
import android.widget.TextView
import android.content.Intent
import android.view.View
import com.manusunny.pinlock.PinListener


class MainActivity : AppCompatActivity()  {

    /**val REQUEST_CODE_SET_PIN = 0
    val REQUEST_CODE_CHANGE_PIN = 1
    val REQUEST_CODE_CONFIRM_PIN = 2
    var pinLockPrefs : SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pin_activity)

        pinLockPrefs = getSharedPreferences("PinLockPrefs", MODE_PRIVATE)

        //Lancer la lecture d'empreinte digital du téléphone
        fab.setOnClickListener {
            val manager = FingerprintManagerCompat.from(this)

            //Démarrer si lecteur d'empreinte retrouvé
            if (manager.isHardwareDetected && manager.hasEnrolledFingerprints()) {
                showFingerprintAuth()
            } else {

                //Affiche si non retrouvé
                Toast.makeText(this, "Pas de lecteur d'empreinte.", Toast.LENGTH_SHORT).show()
            }
        }

        //Implementation du Clavier
        init()

    }

    private fun showFingerprintAuth() {
        val dialog = FingerPrintDialog.newInstance(
            "Empreinte digitale",
            "Placez votre doigt."
        )
        dialog.show(supportFragmentManager, FingerPrintDialog.FRAGMENT_TAG)
    }

    private fun init(){

        val setPin = findViewById<TextView>(R.id.set_pin)
        val confirmPin = findViewById<TextView>(R.id.confirm_pin)

        val pin = pinLockPrefs!!.getString("pin", "")
        if (pin == "") {
            confirmPin.isEnabled = false
        } else {
            setPin.text = "Change PIN"
        }

        val clickListener = getOnClickListener()
        setPin.setOnClickListener(clickListener)
        confirmPin.setOnClickListener(clickListener)
    }

    private fun getOnClickListener(): View.OnClickListener {
        return View.OnClickListener { v ->
            val id = v.id
            val pin = pinLockPrefs!!.getString("pin", "")

            if (id == R.id.set_pin && pin == "") {
                val intent = Intent(this@MainActivity, SetPinCode::class.java)
                startActivityForResult(intent, REQUEST_CODE_SET_PIN)
            } else if (id == R.id.set_pin) {
                val intent = Intent(this@MainActivity, ConfirmCode::class.java)
                startActivityForResult(intent, REQUEST_CODE_CHANGE_PIN)
            } else if (id == R.id.confirm_pin) {
                val intent = Intent(this@MainActivity, ConfirmCode::class.java)
                startActivityForResult(intent, REQUEST_CODE_CONFIRM_PIN)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_SET_PIN -> {
                if (resultCode == PinListener.SUCCESS) {
                    Toast.makeText(this, "Pin is set :)", Toast.LENGTH_SHORT).show()
                } else if (resultCode == PinListener.CANCELLED) {
                    Toast.makeText(this, "Pin set cancelled :|", Toast.LENGTH_SHORT).show()
                }
                refreshActivity()
            }
            REQUEST_CODE_CHANGE_PIN -> {
                if (resultCode == PinListener.SUCCESS) {
                    val intent = Intent(this@MainActivity, SetPinCode::class.java)
                    startActivityForResult(intent, REQUEST_CODE_SET_PIN)
                } else if (resultCode == PinListener.CANCELLED) {
                    Toast.makeText(this, "Pin change cancelled :|", Toast.LENGTH_SHORT).show()
                }
            }
            REQUEST_CODE_CONFIRM_PIN -> {
                if (resultCode == PinListener.SUCCESS) {
                    Toast.makeText(this, "Bienvenue", Toast.LENGTH_SHORT).show()
                } else if (resultCode == PinListener.CANCELLED) {
                    Toast.makeText(this, "Pin confirm cancelled :|", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun refreshActivity() {
        val intent = intent
        finish()
        startActivity(intent)
    }

*/
}
