package com.example.arsenandroid.view.fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.arsenandroid.R
import com.example.arsenandroid.monActivity
import com.example.arsenandroid.view.garanties.*
import kotlinx.android.synthetic.main.mes_garanties.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper

class GarantsFragment:Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.mes_garanties, container, false)

    companion object {
        fun newInstance(): GarantsFragment =
            GarantsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = activity!!.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = activity!!.resources.getColor(R.color.color_white)
            onPause()
        }

        soinsGeneraux.setOnClickListener {
          val intent = Intent(activity, monActivity::class.java)
          startActivity(intent)
        }

        hospitalisation.setOnClickListener {
            val intent = Intent(activity, Hospitalisation::class.java)
            startActivity(intent)
        }

        fraisOptique.setOnClickListener {
            val intent = Intent(activity, FraisOptique::class.java)
            startActivity(intent)
        }

        fraisDentaires.setOnClickListener {
            val intent = Intent(activity, FraisDentaires::class.java)
            startActivity(intent)
        }

        autresSoins.setOnClickListener {
            val intent = Intent(activity, AutresSoins::class.java)
            startActivity(intent)
        }
    }
}