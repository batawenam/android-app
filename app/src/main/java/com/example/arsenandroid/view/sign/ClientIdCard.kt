package com.example.arsenandroid.view.sign


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.models.Cni
import com.example.arsenandroid.utils.APIUtils
import kotlinx.android.synthetic.main.carte_identite_num.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ClientIdCard : AppCompatActivity() {

    val apiService = APIUtils().getAPIService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.carte_identite_num)

        skipButton.setOnClickListener {
            onBackPressed()
        }
        cardValidate.visibility = View.INVISIBLE

        val id = cardId.text
        suivantButton.setOnClickListener {

            if (id.isEmpty()) {
                cardId.error = "Veuillez le numéro"
            }else if (id.isNotEmpty() && id.length < 9){
                cardId.error = "9 caractères au minimum"
            }
            else{
                verifyCard(id.toString())
            }
            val cardSender = Intent(applicationContext, SignUpActivity::class.java)
            cardSender.putExtra("CARD_ID", id)
        }

        cardId.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s!!.length == 9){
                    cardValidate.visibility = View.VISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.length == 9){
                    cardValidate.visibility = View.VISIBLE
                }else{
                    cardValidate.visibility = View.INVISIBLE
                }
            }

        })

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun verifyCard(ci : String){

        apiService.sendCI(ci).enqueue(object : Callback<Cni> {
            override fun onFailure(call: Call<Cni>, t: Throwable) {
                Toast.makeText(applicationContext, "Erreur, veuillez réessayer", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<Cni>, response: Response<Cni>) {
                //startActivity(Intent(applicationContext, Navigation::class.java))

                if (response.code() == 409){

                    Toast.makeText(applicationContext, "Déjà", Toast.LENGTH_SHORT).show()

                    /**val dialog = Dialog(applicationContext)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setContentView(R.layout.already_exist)
                    dialog.findViewById<TextView>(R.id.alreadyKnownText).text = "Nous avons un mail de confirmation au ${response.statusCode}, veuillez confirmer votre compte depuis le mail"
                    dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.show()*/

                }else if(response.code() == 404){
                    val intent = Intent(this@ClientIdCard, ClientEmailSet::class.java)
                    val id = cardId.text.toString()
                    intent.putExtra("CNI", id)
                    startActivity(intent)
                }else if (response.code() == 200){
                    Toast.makeText(applicationContext, "Continuer votre inscription", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this@ClientIdCard, ClientEmailSet::class.java)
                    val id = cardId.text.toString()
                    intent.putExtra("CNI", id)
                    startActivity(intent)
                }
            }

        })



        }
    }