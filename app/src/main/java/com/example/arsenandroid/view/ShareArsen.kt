package com.example.arsenandroid.view

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.DialogFragment
import com.example.arsenandroid.R
import android.widget.Toast


class ShareArsen: DialogFragment() {

    val sharObject : String = "Arsen Assurance Santé, adaptée aux indépendants et aux entreprises"

    val shareText : String = "Arsen Assurance,  c’est ma nouvelle assurance santé, 100% digitale. \n" +
            "C’est des garanties claires et complètes, des médecins à disposition 24h/24 et j’économise près de 300 mille francs l’an. \n" +
            "\n" +
            "Tu vas adorer. \n" +
            "Télécharge l’application via ce lien: www.google.playstore.com/55545151"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.share_dialog, container, false)

        return view
    }


    private fun shareToGmail(){

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/pain"
        intent.setPackage("com.google.android.gm")
        intent.putExtra(Intent.EXTRA_SUBJECT, sharObject)
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        startActivity(intent)

    }

    private fun shareViaWhatP(){

        val whatsappIntent = Intent(Intent.ACTION_SEND)
        whatsappIntent.type = "text/plain"
        whatsappIntent.setPackage("com.whatsapp")
        whatsappIntent.putExtra(Intent.EXTRA_SUBJECT, sharObject)
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, shareText)
        try {
            startActivity(whatsappIntent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(activity, "WhatsApp n'est pas installé", Toast.LENGTH_SHORT).show()
        }

    }

    private fun shareViaSms(){

        val smsIntent = Intent(Intent.ACTION_VIEW)
        smsIntent.data = Uri.parse("smsto:")
        smsIntent.type = "vnd.android-dir/mms-sms"
        smsIntent.putExtra("sms_body", shareText)
        startActivity(smsIntent)

    }

    private fun copyLiink(){

        val clipboardManager = activity!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("Arsen", shareText)

        clipboardManager.primaryClip = clipData

        Toast.makeText(activity, "Text Copié", Toast.LENGTH_SHORT).show()

    }




    override fun onResume() {
        super.onResume()
        dialog?.window?.setContentView(R.layout.share_dialog)
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.show()

        dialog!!.findViewById<ImageButton>(R.id.viaGmail).setOnClickListener {
            shareToGmail()
        }

        dialog!!.findViewById<ImageButton>(R.id.viaWhatsapp).setOnClickListener {
            shareViaWhatP()
        }

        dialog!!.findViewById<ImageButton>(R.id.viaSms).setOnClickListener {
            shareViaSms()
        }

        dialog!!.findViewById<ImageButton>(R.id.viaLink).setOnClickListener {
            copyLiink()
        }
    }

    companion object {

        val FRAGMENT_TAG: String = ShareArsen::class.java.simpleName
        fun newInstance(): ShareArsen = ShareArsen()
    }


}