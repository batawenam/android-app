package com.example.arsenandroid.view.fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.arsenandroid.R
import com.example.arsenandroid.utils.SaveLogged
import kotlinx.android.synthetic.main.edit_secret_code.*
import kotlinx.android.synthetic.main.user_profil.*
import com.example.arsenandroid.view.sign.LogInActivity
import com.example.arsenandroid.view.menu.TransformAsterisk
import kotlinx.android.synthetic.main.change_telephone_number.*
import kotlinx.android.synthetic.main.user_profil.view.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper


class AccountFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.user_profil, container, false)

        view.userName.text = SaveLogged().getFirstName(context!!)
        view.userId.text = SaveLogged().getUserId(context!!)
        view.telSubtitle.text = SaveLogged().getPhone(context!!)
        view.professionSubtitle.text = SaveLogged().getProfession(context!!)
        if (SaveLogged().getGender(context!!) == "M"){
            view.userImg.setImageResource(R.drawable.avatar_boy)
        }else if(SaveLogged().getGender(context!!) == "F"){
            view.userImg.setImageResource(R.drawable.avatar_girl)
        }
        return view
    }

    companion object {
        fun newInstance(): AccountFragment =
            AccountFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        OverScrollDecoratorHelper.setUpOverScroll(overlay)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = activity!!.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = activity!!.resources.getColor(R.color.colorPrimaryDark)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            onPause()
        }

        codeSecretEdit.setOnClickListener {
            var dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.edit_secret_code)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            val showPassword = dialog.findViewById<RelativeLayout>(R.id.showLayout)
            val showOldPass = dialog.findViewById<RelativeLayout>(R.id.oldLayout)
            var initialisation: Boolean = false
            val oldPassAsterisk: EditText = dialog.findViewById(R.id.oldPassword)
            val newPassAsterisk: EditText = dialog.findViewById(R.id.newPassword)
            val confirmAsterisk: EditText = dialog.findViewById(R.id.confirmNewPassword)

            showPassword.setOnClickListener {
                if (initialisation){
                    dialog.confirmNewPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    dialog.newPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    dialog.showConfirm.setImageResource(R.drawable.visible_code)
                    initialisation = false
                } else {
                        dialog.confirmNewPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                        dialog.newPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                       dialog.showConfirm.setImageResource(R.drawable.invisible_code)
                    initialisation = true
                    newPassAsterisk.transformationMethod = TransformAsterisk()
                    confirmAsterisk.transformationMethod = TransformAsterisk()
                }
            }
            showOldPass.setOnClickListener {
                if (initialisation){
                    dialog.oldPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    dialog.showOldPass.setImageResource(R.drawable.visible_code)
                    initialisation = false
                } else {
                    dialog.oldPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                    dialog.showOldPass.setImageResource(R.drawable.invisible_code)
                    initialisation = true
                    oldPassAsterisk.transformationMethod = TransformAsterisk()
                }
            }
            oldPassAsterisk.transformationMethod = TransformAsterisk()
            newPassAsterisk.transformationMethod = TransformAsterisk()
            confirmAsterisk.transformationMethod = TransformAsterisk()

        }

        telEdit.setOnClickListener {
            var dialog = Dialog(activity)
            if (SaveLogged().getPhone(context!!)!!.isNotEmpty()){
                dialog.newTel.setText(SaveLogged().getPhone(context!!))
            }
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.change_telephone_number)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }

        professionEdit.setOnClickListener {
            var dialog = Dialog(activity)
            if (SaveLogged().getProfession(context!!)!!.isNotEmpty()){
                dialog.newTel.setText(SaveLogged().getProfession(context!!))
            }
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.edit_profession)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

        }

        emploisEdit.setOnClickListener {

            var dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.edit_entreprise)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

        }

        emailEdit.setOnClickListener {
            var dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.edit_email)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

        }
    }
}