package com.example.arsenandroid.view

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import com.example.arsenandroid.BuildConfig
import com.example.arsenandroid.CardAdapter
import com.example.arsenandroid.R
import com.wenchao.cardstack.CardStack
import kotlinx.android.synthetic.main.conseils_coach.*

class ConseilsCoach : AppCompatActivity(), CardStack.CardEventListener {
    lateinit var cardStack: CardStack
    lateinit var cardAdapter: CardAdapter

    companion object {
        var progressState = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.conseils_coach)

        initView()

        cardStack = findViewById(R.id.card_view) as CardStack
        cardStack.setContentResource(R.layout.card_layout)
        cardStack.setStackMargin(20)
        cardStack.adapter = cardAdapter
        cardStack.setListener(this)

    }

    private fun initView() {

        cardAdapter = CardAdapter(this, 0)
        cardAdapter.add(R.drawable.ic_man)
        cardAdapter.add(R.drawable.ic_man)
        cardAdapter.add(R.drawable.ic_man)
        cardAdapter.add(R.drawable.ic_man)
        cardAdapter.add(R.drawable.ic_man)

        var cardSize = cardAdapter.count


    }


    override fun swipeStart(section: Int, distance: Float): Boolean {
        cardProgressBar.progress = progressState
        return true
    }

    override fun swipeEnd(section: Int, distance: Float): Boolean {
        cardProgressBar.max = section
        return true
    }

    override fun topCardTapped() {

    }

    override fun swipeContinue(section: Int, distanceX: Float, distanceY: Float): Boolean {
        val progressSize =  cardProgressBar.max
        for (i in 1..section){
            cardProgressBar.progress =+ (progressSize - ((progressSize * (section - 1) / 100)))
        }
        return true
    }

    override fun discarded(mIndex: Int, direction: Int) {

    }

}
