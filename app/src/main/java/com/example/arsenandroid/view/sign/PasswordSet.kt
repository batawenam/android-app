package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.view.menu.TransformAsterisk
import kotlinx.android.synthetic.main.mot_de_passe.*

class PasswordSet : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mot_de_passe)

        skipButton.setOnClickListener {
            onBackPressed()
        }

        val showPassword = findViewById<RelativeLayout>(R.id.showLayout)
        var initialisation: Boolean = false
        val passAsterisk: EditText = findViewById<EditText>(R.id.password)
        val confirmAsterissk: EditText = findViewById<EditText>(R.id.confirmPass)

        showPassword.setOnClickListener {
            if (initialisation) {
                password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                confirmPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                showConfirm.setImageResource(R.drawable.visible_code)
                initialisation = false
            } else {
                password.transformationMethod = PasswordTransformationMethod.getInstance()
                confirmPass.transformationMethod = PasswordTransformationMethod.getInstance()
                showConfirm.setImageResource(R.drawable.invisible_code)
                initialisation = true
                passAsterisk.transformationMethod = TransformAsterisk()
                confirmAsterissk.transformationMethod = TransformAsterisk()
            }
        }
        passAsterisk.transformationMethod = TransformAsterisk()
        confirmAsterissk.transformationMethod = TransformAsterisk()



        confirmAsterissk.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                val text1 = password.text.toString()
                val text2 = confirmPass.text.toString()
                if(text1.isNotEmpty() && text2.isNotEmpty()){
                    if (text2.equals(text1)){
                        valide.visibility = View.VISIBLE
                    }else{
                        valide.visibility = View.INVISIBLE
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val text1 = password.text.toString()
                val text2 = confirmPass.text.toString()

                if (text1.isEmpty()){
                    showPassword.visibility = View.INVISIBLE
                    password.error = "Veuillez entrer un mot de passe"

                }
                if(text1.isNotEmpty() && text2.isNotEmpty()){
                    if (text2.equals(text1)){
                        valide.visibility = View.VISIBLE
                    }else{
                        valide.visibility = View.INVISIBLE
                    }
                }
            }

        })

        suivantButton.setOnClickListener {
            val text1 = password.text.toString()
            val text2 = confirmPass.text.toString()

            if(text1.length < 6 && text2.length < 6){
                password.error = "6 caractères minimum"
                confirmPass.error = "6 caractères minimum"
            }
            else{

                if (text1.isEmpty()){
                    showPassword.visibility = View.INVISIBLE
                    password.error = "Veuillez entrer un mot de passe"
                }
                if (text1.isNotEmpty() && text2.isEmpty()){
                    confirmPass.error = "Veuillez confirmer le mot de passe"
                }
                if (text1.isNotEmpty() && text2.isNotEmpty()){
                    if (text2.equals(text1)){
                        val intent = Intent(this@PasswordSet, SignUpActivity::class.java)
                        intent.putExtra("PASSWORD", text1)
                        startActivity(intent)
                    }else{
                        confirmPass.error = "Le mot de passe ne correspond pas"
                        valide.visibility = View.INVISIBLE
                    }
                }

            }


            Log.d("debug", text1)
        }

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }
}