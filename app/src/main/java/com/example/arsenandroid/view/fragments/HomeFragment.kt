package com.example.arsenandroid.view.fragments


import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.arsenandroid.R
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.view.MyArsenCard
import com.example.arsenandroid.view.ShareArsen
import com.example.arsenandroid.view.SocialCause
import kotlinx.android.synthetic.main.home_page.*
import kotlinx.android.synthetic.main.home_page.view.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import org.apache.commons.lang3.StringUtils
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.example.arsenandroid.view.sign.ResetPassword
import kotlinx.android.synthetic.main.download_gomediacal.*


class HomeFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View?{
        val view: View = inflater.inflate(R.layout.home_page, container, false)
        view.userName.text = SaveLogged().getFirstName(context!!)
        view.lastName.text = SaveLogged().getlastName(context!!)
        view.userID.text = SaveLogged().getUserId(context!!)
        if (SaveLogged().getGender(context!!) == "M"){
            view.userPhoto.setImageResource(R.drawable.avatar_boy)
        }else if(SaveLogged().getGender(context!!) == "F"){
            view.userPhoto.setImageResource(R.drawable.avatar_girl)
        }

        var time = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        if (time < 12){
            view.greeting.text = "Bonjour !"
        }else if (time >= 12){
            view.greeting.text = "Bonsoir !"
        }
        return view
    }

    companion object {
        fun newInstance(): HomeFragment =
            HomeFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        OverScrollDecoratorHelper.setUpOverScroll(overlay)

        val myDate: Long = System.currentTimeMillis()
        val sdf = SimpleDateFormat("EEEE, dd")
        val dateString: String = sdf.format(myDate)
        currentDate.text = dateString
        currentDate.text = StringUtils.capitalize(dateString.toLowerCase().trim())

        val monthDate: Long = System.currentTimeMillis()
        val sdfMonth = SimpleDateFormat("MMM")
        val monthString: String = sdfMonth.format(monthDate)
        secondCurrentDate.text = monthString
        secondCurrentDate.text = StringUtils.capitalize(monthString.toLowerCase().trim())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = activity!!.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = activity!!.resources.getColor(R.color.color_white)
        }

        arsenCarte.setOnClickListener {
            startActivity(Intent(activity, MyArsenCard::class.java))

        }


        shareArsen.setOnClickListener {
            val shareDialog = ShareArsen.newInstance()
            val fManager = activity!!.supportFragmentManager
            shareDialog.show(fManager, ShareArsen.FRAGMENT_TAG)
        }

        causeSocial.setOnClickListener {
            startActivity(Intent(activity, SocialCause::class.java))
        }

        consultDoc.setOnClickListener {

                val intent = activity!!.packageManager.getLaunchIntentForPackage("co.opensi.medical")
                if(intent != null){
                    startActivity(intent)
                }else {
                    var dialog = Dialog(activity)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setContentView(R.layout.download_gomediacal)
                    dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.downloadBtn.setOnClickListener { startActivity(Intent(activity, ResetPassword::class.java)) }
                    dialog.show()
                }

        }
    }

}