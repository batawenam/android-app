package com.example.arsenandroid.view.sign

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.utils.APIUtils
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.view.menu.Navigation
import kotlinx.android.synthetic.main.connexion.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LogInActivity : AppCompatActivity() {

    val apiService = APIUtils().getAPIService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.connexion)

        connexionButton.setOnClickListener {
            val identifiant = telEMail.text.toString()
            val password = passwordEdit.text.toString()
            if(identifiant.isEmpty() && password.isEmpty()){
                telEMail.error = "Veuillez entrer un email ou un numéro"
                passwordEdit.error = "Veuillez entrer un email ou un numéro"
            }else if (identifiant.isEmpty()){
                telEMail.error = "Entrer votre email ou votre numéro"
            }else if (password.isEmpty()) {
                passwordEdit.error = "Entrer un mot de passe"
            }else if (password.isNotEmpty() && password.length < 6){
                passwordEdit.error = "6 caractères minimum"
            }else {
                val cContext = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = cContext.activeNetworkInfo
                if (networkInfo != null && networkInfo.isConnected){
                logInRequest(identifiant, password)
                }
                else{
                    //Snackbar.SnackbarLayout(applicationContext)
                    Toast.makeText(this, "Veuillez vérifier votre connexion internet!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        passwordForgot.setOnClickListener {
            startActivity(Intent(applicationContext, ResetPassword::class.java))
        }

        inscriptionButton.setOnClickListener {
            startActivity(Intent(applicationContext, ClientStatus::class.java))
        }

        if (SaveLogged().getIdentity(this) != null || SaveLogged().getIdentity(this).equals("")){
            startActivity(Intent(this, Navigation::class.java))
            finish()
        }
    }

    fun logInRequest(identifiant: String, password : String){

        apiService.logUser(identifiant, password).enqueue(object : Callback<Any>{

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(applicationContext, "Erreur, veuillez réessayer ${t.cause}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful) {
                    val jsonObject = JSONObject(response.body() as Map<*, *>)

                    firstName = jsonObject.getString("firstName")
                    lastName = jsonObject.getString("lastName")
                    gender = jsonObject.getString("gender")
                    cni = jsonObject.getString("ci")
                    residence = jsonObject.getString("residencePlace")
                    profession = jsonObject.getString("profession")
                    tel = jsonObject.getString("phone")
                    idUser = jsonObject.getString("userId")


                    startActivity(Intent(applicationContext, Navigation::class.java))
                    finish()
                }
                SaveLogged().saveIdentity(identifiant, this@LogInActivity)
                SaveLogged().savePassword(password, this@LogInActivity)
                SaveLogged().saveFirstName(firstName, this@LogInActivity)
                SaveLogged().savelastName(lastName, this@LogInActivity)
                SaveLogged().saveGender(gender, this@LogInActivity)
                SaveLogged().saveProfession(profession, this@LogInActivity)
                SaveLogged().savePhone(tel, this@LogInActivity)
                SaveLogged().saveUserId(idUser, this@LogInActivity)
                SaveLogged().saveUserId(idUser, this@LogInActivity)
                SaveLogged().saveUserId(idUser, this@LogInActivity)
            }

        })
    }

    companion object {
        var firstName = ""
        var lastName = ""
        var gender = ""
        var cni = ""
        var residence = ""
        var profession = ""
        var tel = ""
        var idUser = ""
        var cardId = ""
    }

}