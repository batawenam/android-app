package com.example.arsenandroid.view.search

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.arsenandroid.R



class SearchBar : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_bar)

        val toolbar = findViewById<Toolbar>(R.id.searchLayout)
        setSupportActionBar(toolbar)
    }

}