package com.example.arsenandroid.view.garanties

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.soins_generaux.*

class SoinsGeneraux : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.soins_generaux)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        skipButton.setOnClickListener {
            onBackPressed()
        }

        /*var consulGeneText: TextView = findViewById(R.id.consulGeneText)
        var consulGeneSubtitle: TextView = findViewById(R.id.consulGeneSubtitle)
        var consulGeneImg: ImageView = findViewById(R.id.consulGeneImg)*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }

        /*consulGeneraliste.setOnClickListener {
            setContentView(R.layout.garanties_details)
            var text: TextView = findViewById(R.id.itemText)
            var subtitle: TextView = findViewById(R.id.itemSubtitle)
            text.text = consulGeneText.text
            subtitle.text = consulGeneSubtitle.text
        }*/

    }

}
