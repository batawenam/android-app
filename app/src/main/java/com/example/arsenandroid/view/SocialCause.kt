package com.example.arsenandroid.view

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.arsenandroid.R
import com.example.arsenandroid.models.CauseModel
import com.example.arsenandroid.utils.APIUtils
import com.example.arsenandroid.utils.PrefConstants
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.utils.StoreCauses
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*




class SocialCause : AppCompatActivity() {

    val apiService = APIUtils().getAPIService()

    var recycler : RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.socialcause_screen)

        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@SocialCause)
        val gson = Gson()
        val json = preferences.getString(PrefConstants().CAUSES, null)
        val type = object : TypeToken<List<CauseModel>>() {
        }.type
        var causes: List<CauseModel> = gson.fromJson(json, type)

        if (causes.isEmpty()){
            retrieveCauses()
        }

        recycler = findViewById<RecyclerView>(R.id.causeRecycler)
        recycler!!.hasFixedSize()
        recycler!!.layoutManager = LinearLayoutManager(this)
        val mAdapter = CauseAdapter(causes, applicationContext)
        recycler!!.adapter = mAdapter

    }

    fun retrieveCauses(){
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val uId = SaveLogged().getUserId(this)!!

        apiService.getCauses(uId, currentYear).enqueue(object : Callback<List<CauseModel>> {

            override fun onFailure(call: Call<List<CauseModel>>, t: Throwable) {
                Toast.makeText(applicationContext, "Erreur, veuillez réessayer ${t.cause}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<CauseModel>>, response: Response<List<CauseModel>>) {
                if (response.isSuccessful) {
                    val causeList = response.body()
                    StoreCauses().saveList(causeList!!, this@SocialCause)

                }
            }

        })
    }

}