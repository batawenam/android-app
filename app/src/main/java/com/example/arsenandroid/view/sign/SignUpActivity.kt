package com.example.arsenandroid.view.sign

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.models.SignUpModel
import com.example.arsenandroid.utils.APIUtils
import com.example.arsenandroid.view.menu.Navigation
import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.infos_personnelles.*
import okhttp3.Cookie
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener  {

    var villes = ArrayList<String>()
    var title = ""
    val apiService = APIUtils().getAPIService()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.infos_personnelles)

        skipButton.setOnClickListener {
            onBackPressed()
        }
        villes.add("Cotonou")
        villes.add("Abomey-Calavi")
        villes.add("Porto-Novo")
        villes.add("Parakou")
        villes.add("Dougou")
        villes.add("Bohicon")
        villes.add("Natitingou")
        villes.add("Savé")
        villes.add("Abomey")
        villes.add("Nikki")
        villes.add("Lokossa")
        villes.add("Ouidah")
        villes.add("Dogbo-Tota")
        villes.add("Kandi")
        villes.add("Covè")
        villes.add("Malanville")
        villes.add("Pobè")
        villes.add("Kérou")
        villes.add("Savalou")
        villes.add("Sakété")
        villes.add("Comè")
        villes.add("Bembéréké")
        villes.add("Bassila")
        villes.add("Banikoara")
        villes.add("Kétou")
        villes.add("Dassa-Zoumè")
        villes.add("Tchaorou")
        villes.add("Allada")
        villes.add("Aplahoué")
        villes.add("Tanguiéta")
        villes.add("N'Dali")
        villes.add("Segbanan")
        villes.add("Athiémé")
        villes.add("Grand-Popo")
        villes.add("Kouandé")

        val autoCompleteTxt = findViewById(R.id.autoCompleteTxt) as AutoCompleteTextView
        val adapter = ArrayAdapter<String>(this, android.R.layout.select_dialog_item, villes)
        autoCompleteTxt.threshold = 1
        autoCompleteTxt.setAdapter(adapter)

        val spinner = findViewById<Spinner>(R.id.spinner)
        val sAdapter = ArrayAdapter.createFromResource(this, R.array.personne_titles, android.R.layout.simple_spinner_item)
        sAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        spinner.adapter = sAdapter
        spinner.onItemSelectedListener = this

        signUpButton.setOnClickListener {

            verify()

            val password: String = intent.getStringExtra("PASSWORD")
            val firstName = firstName.text.toString()
            val lastName = lastname.text.toString()
            val gender: String = spinner.selectedItem.toString()
            val birthday  = age.text.toString()
            val residencePlace = autoCompleteTxt.text.toString()
            val profession: String = work.text.toString()
            val entreprise: String = workPlace.text.toString()
            val accountId = ClientEmailSet.accountid

            val userDatas = SignUpModel(
                password, firstName, lastName,
                gender, birthday, residencePlace, profession
            )

            apiService.sendDatas(firstName, lastName, password, gender,
                birthday, residencePlace, profession, entreprise, accountId).enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                    Toast.makeText(applicationContext, "Erreur, veuillez réessayer ${t.cause}", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {

                    if (response.code() == 200 || response.code() == 500) {
                        val jsonObject = JSONObject(response.body() as Map<*, *>)
                        val intent = Intent(this@SignUpActivity, LogInActivity::class.java)
                        startActivity(intent)

                    } else {
                        Toast.makeText(applicationContext, "Erreur, veuillez réessayer ${response.code()}", Toast.LENGTH_SHORT).show()
                    }

                }

            })

        }

    }

    fun verify(){
        val prenom = firstName.text.toString()
        val nom = lastname.text.toString()
        val birth = age.text.toString()
        val residence = autoCompleteTxt.text.toString()
        val uWork = work.text.toString()

        if (prenom.isEmpty()){
            firstName.error = "Veuillez remplir tout les champs"
        }else if (nom.isEmpty()){
            lastname.error = "Veuillez remplir tout les champs"
        }else if (birth.isEmpty()){
            age.error = "Veuillez remplir tout les champs"
        }else if (residence.isEmpty()){
            autoCompleteTxt.error = "Veuillez remplir tout les champs"
        }else if (uWork.isEmpty()){
            work.error = "Veuillez remplir tout les champs"
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    fun phoneSignUpRequest(user: SignUpModel){

        val postJson = Gson().toJson(user)
        Log.d("debug", postJson)

        val cookie : Cookie


    }

}