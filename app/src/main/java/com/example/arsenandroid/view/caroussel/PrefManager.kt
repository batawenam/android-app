package com.example.arsenandroid.views.caroussel

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.arsenandroid.R
import com.example.arsenandroid.homeScreen
import com.example.arsenandroid.view.menu.Navigation

class PrefManager {

    lateinit var con: Context
    lateinit var preferences: SharedPreferences

    constructor(con: Context) {
        this.con = con
        getSP()
    }

    private fun getSP() {
        preferences = con.getSharedPreferences(con.getString(R.string.pref_name), Context.MODE_PRIVATE)
    }

    fun writeSP() {
        var editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(con.getString(R.string.pref_key), "Suivant")
        editor.commit()
    }

    fun checkPreference(): Boolean {
        var status: Boolean = false
        if (preferences.getString(con.getString(R.string.pref_key), "").equals("")) {
            status = false
        } else {
            status = true
        }
        return status
    }

    fun clearPreference() {
        preferences.edit().clear().commit()
        con.startActivity(Intent(con, Navigation::class.java))
        (con as Navigation).finish()
    }
}