package com.example.arsenandroid.view.caroussel

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.example.arsenandroid.R
import com.example.arsenandroid.homeScreen
import com.example.arsenandroid.view.menu.Navigation
import com.example.arsenandroid.view.sign.LogInActivity
import com.example.arsenandroid.views.caroussel.PageAdapter
import com.example.arsenandroid.views.caroussel.PrefManager

class CarousselActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mpager: ViewPager
    var layouts: IntArray =
        intArrayOf(R.layout.first_slide, R.layout.second_slide, R.layout.third_slide)
    lateinit var dotsLayout: LinearLayout
    lateinit var dots: Array<ImageView>
    lateinit var mAdapter: PageAdapter
    lateinit var btnPrev: Button
    lateinit var btnNext: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 19) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
        setContentView(R.layout.caroussel_base)
        mpager = findViewById<ViewPager>(R.id.pager) as ViewPager
        mAdapter = PageAdapter(layouts, this)
        mpager.adapter = mAdapter
        dotsLayout = findViewById<LinearLayout>(R.id.dots) as LinearLayout
        btnPrev = findViewById<Button>(R.id.prevButton) as Button
        btnNext = findViewById<ImageButton>(R.id.nextButton) as ImageButton
        btnPrev.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        createDots(0)
        mpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                createDots(position)

                if (position == layouts.size - 1) {
                    btnPrev.visibility = View.INVISIBLE
                } else {
                    btnNext.visibility = View.VISIBLE
                    btnPrev.visibility = View.VISIBLE
                }
            }

        })


    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.prevButton -> {
                loadPrevSlide()
            }
            R.id.nextButton -> {
                loadNextSlide()
            }
        }
    }

    private fun loadNextSlide() {
        var nextSlide: Int = mpager.currentItem + 1
        if (nextSlide < layouts.size) {
            mpager.currentItem = nextSlide
        } else {
            loadNextStep()
            PrefManager(this).writeSP()
        }
    }

    private fun loadPrevSlide() {
        var prevSlide: Int = mpager.currentItem - 1
        if (prevSlide < layouts.size) {
            mpager.currentItem = prevSlide
        }
    }

    private fun loadNextStep() {
        startActivity(Intent(this, LogInActivity::class.java))
        finish()
    }

    fun createDots(position: Int) {
        if (dotsLayout !== null) {
            dotsLayout.removeAllViews()
        }
        dots = Array(layouts.size) { i -> ImageView(this) }
        for (i in 0..layouts.size - 1) {
            dots[i] = ImageView(this)
            if (i == position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots))
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.inactive_dots))
            }
            var params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(2, 4, 2, 4)
            dotsLayout.addView(dots[i], params)
        }
    }
}