package com.example.arsenandroid.view

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.ViewGroup
import android.view.Window
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.arsenandroid.R
import com.example.arsenandroid.beneficiaire.Invitation
import com.example.arsenandroid.view.sign.AddFamily
import kotlinx.android.synthetic.main.add_benef_dialog.*
import kotlinx.android.synthetic.main.arsen_card.*

class MyArsenCard : AppCompatActivity() {

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing){
            YoYo.with(Techniques.BounceInRight)
                .duration(600)
                .repeat(0)
                .playOn(arsenCardCardView)

            YoYo.with(Techniques.FadeInUp)
                .duration(600)
                .repeat(0)
                .playOn(benefInfoSuppl)

        }
    }
    private var mDelayHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.arsen_card)

        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, 300)

        val parent: ViewGroup = benefName.parent as ViewGroup
        parent.removeView(benefName)

        addBeneficiaire.setOnClickListener {
            var dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.add_benef_dialog)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            dialog.assurOtherPerson.setOnClickListener {
                startActivity(Intent(this, AddFamily::class.java))
            }

            dialog.inviteBenef.setOnClickListener {
                startActivity(Intent(this, Invitation::class.java))
            }
        }

        skipButton.setOnClickListener {
            onBackPressed()
        }

    }
}
