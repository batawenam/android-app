package com.example.arsenandroid.view.sign

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.example.arsenandroid.utils.APIUtils
import kotlinx.android.synthetic.main.reset_password.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPassword : AppCompatActivity() {

    val apiService = APIUtils().getAPIService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reset_password)

        resetResultLayout.visibility = View.INVISIBLE

        val identifiant = eDt.text


        searchBtn.setOnClickListener {
            if (identifiant.toString().isEmpty()){
                eDt.error = "Veuillez entrer un email ou un numéro"
            }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(identifiant).matches() && !android.util.Patterns.PHONE
                    .matcher(identifiant).matches()){
                eDt.error = "Email ou numéro incorrect"
            }
            else {
                verifyAccount(identifiant.toString())
            }
        }

        skipButton.setOnClickListener {
            onBackPressed()
        }

    }

    fun verifyAccount(identifiant: String){

        val cContext = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cContext.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected){

        apiService.verifyAccount(identifiant).enqueue(object : Callback<Any> {

            override fun onFailure(call: Call<Any>, t: Throwable) {
                resetResultLayout.visibility = View.VISIBLE
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful) {
                    val jsonObject = JSONObject(response.body() as Map<*, *>)
                    resetResultLayout.visibility = View.INVISIBLE
                    eDt.visibility = View.INVISIBLE
                    searchBtn.text = "Réinitialiser"
                    resetResultSuccess.visibility = View.VISIBLE
                    resetResultSuccess.setOnClickListener {
                        startActivity(Intent(applicationContext, ConfirmationCode::class.java))
                    }

                }
            }

        })}
        else{
            //Snackbar.SnackbarLayout(applicationContext)
            Toast.makeText(this, "Veuillez vérifier votre connexion internet!", Toast.LENGTH_SHORT).show()
        }
    }


}