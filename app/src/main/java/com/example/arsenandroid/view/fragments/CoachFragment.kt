package com.example.arsenandroid.view.fragments

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.fragment.app.Fragment
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.coach_screen_step1.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper

class CoachFragment:Fragment() {

    private val mRunnable: Runnable = Runnable {
        var parent: ViewGroup = coachCard.parent as ViewGroup
        parent.removeView(coachCard)
        YoYo.with(Techniques.FadeOutRight)
            .duration(600)
            .repeat(0)
            .playOn(coachCard)

        YoYo.with(Techniques.FadeInUp)
            .duration(600)
            .repeat(0)
            .playOn(replaceCoachCard)

    }
    private var mDelayHandler: Handler? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.coach_screen_step1, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        OverScrollDecoratorHelper.setUpOverScroll(overlay)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window: Window = activity!!.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = activity!!.resources.getColor(R.color.color_white)
            onPause()
        }

        understand.setOnClickListener {

            mDelayHandler = Handler()
            //Navigate with delay
            mDelayHandler!!.postDelayed(mRunnable, 300)
            //PrefManager(activity!!).writeSP()

        }
    }

    companion object {
        fun newInstance(): CoachFragment =
            CoachFragment()
    }

}