package com.example.arsenandroid.view.garanties

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.hospitalisation.*

class Hospitalisation : AppCompatActivity()/*, View.OnClickListener */ {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hospitalisation)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        skipButton.setOnClickListener {
            onBackPressed()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = this.window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = this.resources.getColor(R.color.color_white)
            onPause()
        }

        // Passer les détails de  honoraires et actes  dans l'activité garantie détails
        val title1 = findViewById<TextView>(R.id.honoActesText)
        val text1 = findViewById<TextView>(R.id.honoActesSubtitle)
        val layout1 = findViewById<LinearLayout>(R.id.honorairesActes)
        layout1.setOnClickListener {
            val string: String = title1.text.toString()
            val secondString: String = text1.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.soins_generaux_icon)
            startActivity(intent)
        }

        // Passer les détails de maternité  dans l'activité garantie détails
        val title2 = findViewById<TextView>(R.id.materniteText)
        val text2 = findViewById<TextView>(R.id.materniteSubtitle)
        val layout2 = findViewById<LinearLayout>(R.id.maternite)
        layout2.setOnClickListener {
            val string: String = title2.text.toString()
            val secondString: String = text2.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.hospitalisation_icon)
            startActivity(intent)
        }

        // Passer les détails de frais de séjour  dans l'activité garantie détails
        val title3 = findViewById<TextView>(R.id.fraisSejourText)
        val text3 = findViewById<TextView>(R.id.fraisSejourSubtitle)
        val layout3 = findViewById<LinearLayout>(R.id.fraisSejour)
        layout3.setOnClickListener {
            val string: String = title3.text.toString()
            val secondString: String = text3.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.frais_optique_icon)
            startActivity(intent)
        }

        // Passer les détails de  forfait journalier  dans l'activité garantie détails
        val title4 = findViewById<TextView>(R.id.forJourText)
        val text4 = findViewById<TextView>(R.id.forJourSubtitle)
        val layout4 = findViewById<LinearLayout>(R.id.forfaitJournalier)
        layout4.setOnClickListener {
            val string: String = title4.text.toString()
            val secondString: String = text4.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.frais_dentaires_icon)
            startActivity(intent)
        }

        // Passer les détails de  honoraires et actes  dans l'activité garantie détails
        val title5 = findViewById<TextView>(R.id.litAccompText)
        val text5 = findViewById<TextView>(R.id.litAccompSubtitle)
        val layout5 = findViewById<LinearLayout>(R.id.litAccompagnant)
        layout5.setOnClickListener {
            val string: String = title5.text.toString()
            val secondString: String = text5.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.soins_generaux_icon)
            startActivity(intent)
        }

        // Passer les détails de  Transport0  dans l'activité garantie détails
        val title6 = findViewById<TextView>(R.id.transportText)
        val text6 = findViewById<TextView>(R.id.transportSubtitle)
        val layout6 = findViewById<LinearLayout>(R.id.transport)
        layout6.setOnClickListener {
            val string: String = title6.text.toString()
            val secondString: String = text6.text.toString()
            //var image: Int = img as Int
            val intent = Intent(this, GarantiesDetails::class.java)
            intent.putExtra("title", string)
            intent.putExtra("text", secondString)
            intent.putExtra("resId", R.drawable.autres_soins_icon)
            startActivity(intent)
        }
    }

    /*byte[] byteArray = getIntent().getExtras().getByteArray("image");
Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
img.setImageBitmap(bmp);*/

    /*override fun onClick(view: View) {
        when(view.id){
            R.id.honorairesActes -> {
                if (view.tag != null){
                    showGarantieDetails(view.tag as Int)
                }

            }
        }
    }

    fun showGarantieDetails(garantieIndex: Int){
        val garant = garanties[garantieIndex]
        val intent = Intent(this, GarantiesDetails::class.java)
        intent.putExtra(GarantiesDetails.EXTRA_GARANTIE, garant)
        intent.putExtra(GarantiesDetails.EXTRA_GARANTIE_INDEX, garantieIndex)
        startActivity(intent)
    }*/

}
