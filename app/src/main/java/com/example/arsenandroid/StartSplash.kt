package com.example.arsenandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.arsenandroid.utils.StorePin
import com.example.arsenandroid.view.menu.Navigation
import com.example.arsenandroid.view.pin.CheckPin
import com.example.arsenandroid.view.pin.PinLock
import kotlinx.android.synthetic.main.start_splash.*

class StartSplash : AppCompatActivity() {

    private var mDelayHandler: Handler? = null
    private var SPLASH_DELAY: Long = 2000 // 2 seconds
    private val mRunnable: Runnable = Runnable {
        if (!isFinishing){
            val state = "CONFIRMED"
            if (StorePin().getState(this@StartSplash) != null && StorePin().getState(this@StartSplash) == state) {
                val intent = Intent(this@StartSplash, CheckPin::class.java)
                startActivity(intent)
                finish()
            }else if (StorePin().getState(this@StartSplash) != null || StorePin().getState(this@StartSplash) != state){
                startActivity(Intent(this@StartSplash, PinLock::class.java))
                finish()
            }
        }
    }
    var progressState = 0
    var isStated = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_splash)

        overlay.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        //Initialize the handler
        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

        mDelayHandler = Handler(Handler.Callback {
            if (isStated){
                progressState += 5
            }
            progressBar.progress = progressState
            mDelayHandler?.sendEmptyMessageDelayed(0, 100)
            true
        })
        mDelayHandler?.sendEmptyMessage(0)

    }

    public override fun onDestroy() {
        if (mDelayHandler != null){
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}
