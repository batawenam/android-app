package com.example.arsenandroid.chat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.arsenandroid.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class ChatActivity : AppCompatActivity(){


    private var auth : FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.message_list)

        auth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth!!.currentUser
        if (currentUser != null) {

        }else{
            auth!!.signInAnonymously().addOnCompleteListener(this) {
                task ->  if (task.isSuccessful){
                val user = auth!!.currentUser
                user?.let {

                }
            }
            }
        }
    }

}