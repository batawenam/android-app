package com.example.arsenandroid.controller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import co.opensi.kkiapay.uikit.Kkiapay
import com.example.arsenandroid.R
import kotlinx.android.synthetic.main.payement_mode.*

class Payement : AppCompatActivity() {

    private lateinit var notificationManager: NotificationManagerCompat
    private lateinit var notificationText: EditText
    private lateinit var notificationTitle: EditText

    var CHANNEL_ID: String = "Channel"
    private val KEY_TEXT_REPLY = "key_text_reply"
    private var mDelayHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payement_mode)


    }


    /*
    Notification standard

    monImage.setOnClickListener {

            YoYo.with(Techniques.ZoomInRight)
                .duration(500)
                .repeat(0)
                .playOn(monImage)

            //startActivity(Intent(this, MyArsenCard::class.java))
            /*var a: Animation = AnimationUtils.loadAnimation(this, R.anim.slide_down)
            monImage.startAnimation(a)
            monImage.visibility = View.INVISIBLE

            mDelayHandler = Handler()
            //Navigate with delay
            mDelayHandler!!.postDelayed(mRunnable, 600)*/
        }

        /*notificationManager = NotificationManagerCompat.from(this)
        notificationText = findViewById(R.id.notif)
        notificationTitle = findViewById(R.id.notifEssai)*/


    private fun sendChannel(){

        val title: String = notificationTitle.text.toString()
        val text: String = notificationText.text.toString()

        val intent: Intent = Intent(this, AddFamily::class.java)
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(this, 0, intent, 0)

        val broscastIntent: Intent = Intent(this, NotificationReceive::class.java)
        broscastIntent.putExtra("toastMessage", text)
        val actionIntent: PendingIntent = PendingIntent.getBroadcast(this, 0,
            broscastIntent, PendingIntent.FLAG_UPDATE_CURRENT)


        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.logo_accueil)
            .setContentTitle(title)
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            //.setColor(Color.BLUE)
            // Set the intent that will fire when the user taps the notification
            .setAutoCancel(true)
            //.setOnlyAlertOnce(true)
            //.addAction(R.drawable.age_icon, "toast", actionIntent)
            .setContentIntent(pendingIntent)
            .addAction(R.drawable.ic_check, getString(R.string.le_coach),
                pendingIntent)

        with(NotificationManagerCompat.from(this)){
            notify(1, builder.build())
        }

    }*/

    /*
    Reponse à la conversation depuis notification

    private fun receiveMessage(){
        val title: String = notificationTitle.text.toString()
        val text: String = notificationText.text.toString()

        val intent: Intent = Intent(this, AddFamily::class.java)
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(this, 0, intent, 0)

        // Key for the string that's delivered in the action's intent.
        var replyLabel: String = resources.getString(R.string.more)
        var remoteInput: RemoteInput = RemoteInput.Builder(KEY_TEXT_REPLY).run {
            setLabel(replyLabel)
            build()
        }
        // Build a PendingIntent for the reply action to trigger.
        var replyIntent: Intent =
            Intent(this, NotificationReceive::class.java)
        var replyPendingIntent: PendingIntent = PendingIntent.getBroadcast(this, 0,
            replyIntent, 0)

        // Create the reply action and add the remote input.
        var action: NotificationCompat.Action =
            NotificationCompat.Action.Builder(R.drawable.logo_accueil,
                getString(R.string.more), replyPendingIntent)
                .addRemoteInput(remoteInput)
                .build()

        var messageStyle: NotificationCompat.MessagingStyle = NotificationCompat.MessagingStyle("Me")
        messageStyle.conversationTitle = "Arsen chat"

        // Build the notification and add the action.
        val newMessageNotification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.logo_accueil)
            .setStyle(messageStyle)
            .setContentTitle(title)
            .setContentText(text)
            .addAction(action)
            .build()

// Issue the notification.
        NotificationManagerCompat.from(this).apply {
            notificationManager.notify(1, newMessageNotification)
        }

    }*/

    /*private fun getMessageText(intent: Intent): CharSequence? {
        return RemoteInput.getResultsFromIntent(intent)?.getCharSequence(KEY_TEXT_REPLY)
    }*/

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Kkiapay.get().handleActivityResult(requestCode, resultCode, data)
    }*/
}
