package com.example.arsenandroid.controller

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class NotificationReceive: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        var message: String = intent!!.getStringExtra("coachMessage")
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}