package com.example.arsenandroid.controller

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import co.opensi.kkiapay.uikit.Kkiapay
import co.opensi.kkiapay.uikit.SdkConfig
import com.example.arsenandroid.R

class App : Application() {

    /*companion object {
        val CHANNEL_ID: String = "Channel"
    }*/
    override fun onCreate() {
        super.onCreate()

        Kkiapay.init(applicationContext, "31620120474611e9a449779589c13cfb",
            SdkConfig(themeColor = R.color.colorPrimary,
                imageResource = R.drawable.logo_kkia_pay)
        )

    }

    /*private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(
                CHANNEL_ID,
                "channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "Voici une notification"
            var manager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
        //createNotificationChannel()
    }*/

}