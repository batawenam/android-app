package com.example.arsenandroid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.core.os.CancellationSignal
import com.example.arsenandroid.utils.SaveLogged
import com.example.arsenandroid.view.menu.Navigation
import com.example.arsenandroid.view.sign.LogInActivity

class FingerprintHandler (private val fingerprintManager: FingerprintManagerCompat,
                          private val callback: Callback,
                          private val subtitle: TextView,
                          private val errorText: TextView,
                          private val icon: ImageView
) : FingerprintManagerCompat.AuthenticationCallback() {



    private val context: Context
        get() = errorText.context

    private var cancellationSignal: CancellationSignal? = null
    private var selfCancelled = false

    private val isFingerprintAuthAvailable: Boolean
        get() = fingerprintManager.isHardwareDetected && fingerprintManager.hasEnrolledFingerprints()

    private val resetErrorTextRunnable: Runnable = Runnable {
        errorText.setTextColor(ContextCompat.getColor(context, R.color.color_white))
        errorText.text = context.getString(R.string.fingerprint)
        icon.setImageResource(R.drawable.ic_fingerprint)
    }

    init {
        errorText.post(resetErrorTextRunnable)
    }

    fun startListening(cryptoObject: FingerprintManagerCompat.CryptoObject) {
        if (!isFingerprintAuthAvailable) return

        cancellationSignal = CancellationSignal()
        selfCancelled = false
        fingerprintManager.authenticate(cryptoObject, 0, cancellationSignal, this, null)
    }

    fun stopListening() {
        cancellationSignal?.let {
            selfCancelled = true
            it.cancel()
            cancellationSignal = null
        }
    }

    private fun showError(text: CharSequence?) {
        icon.setImageResource(R.drawable.ic_error)
        errorText.text = text
        errorText.setTextColor(ContextCompat.getColor(errorText.context, R.color.color_red))
        errorText.removeCallbacks(resetErrorTextRunnable)
        errorText.postDelayed(resetErrorTextRunnable, ERROR_TIMEOUT_MILLIS)
    }

    override fun onAuthenticationError(errMsgId: Int, errString: CharSequence?) {
        if (!selfCancelled) {
            showError(errString)
            icon.postDelayed({
                callback.onError()
            }, ERROR_TIMEOUT_MILLIS)
        }
    }

    override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
        errorText.removeCallbacks(resetErrorTextRunnable)
        icon.setImageResource(R.drawable.ic_check)
        errorText.setTextColor(ContextCompat.getColor(errorText.context, R.color.colorPrimary))
        errorText.text = errorText.context.getString(R.string.fingerprint_recognized)
        icon.postDelayed({
            callback.onAuthenticated()
        }, SUCCESS_DELAY_MILLIS)
        if (SaveLogged().getFirstName(context) != null && SaveLogged().getFirstName(context)!!.isNotEmpty()
            && SaveLogged().getlastName(context)!!.isNotEmpty()) {
            val intent = Intent(context, Navigation::class.java)
            startActivity(context, intent, Bundle())

        }else if (SaveLogged().getFirstName(context) == null || (SaveLogged().getFirstName(context)!!.isEmpty()
                    && SaveLogged().getlastName(context)!!.isEmpty())){
            val intent = Intent(context, LogInActivity::class.java)
            startActivity(context, intent, Bundle())
        }


    }


    override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence?) {
        showError(helpString)
    }

    override fun onAuthenticationFailed() {
        showError(errorText.context.getString(R.string.fingerprint_not_recognized))
    }

    companion object {
        private val ERROR_TIMEOUT_MILLIS = 1600L
        private val SUCCESS_DELAY_MILLIS = 1300L
    }

    interface Callback {
        fun onAuthenticated()
        fun onError()}

}
